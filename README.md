# Music Player

_组员：罗浩、李兴男_

仓库：[https://jihulab.com/javastudy2/musicplayer.git](https://jihulab.com/javastudy2/musicplayer.git)

开发工具：Intellij IDEA Ultimate 2022.3.3

开发套件：JDK20

运行环境要求：JRE17或JDK17以上

项目进度：

- [ ] 已完成
- [x] 未完成但能运行
- [ ] 不能运行

## 概述：

项目正在开发中，目前主程序完成了

- [x] 精美的美术UI
- [x] 数据库功能
- [x] 连接服务器功能
- [x] 歌单下载展示功能
- [x] 所有音乐展示功能
- [x] 歌单加入播放列表播放功能
- [x] 双击歌单中的单曲加入播放列表功能（包含查重）
- [x] 列表自动循环功能
- [x] 上一首/下一首
- [x] 在播放列表中双击切换歌曲
- [x] 暂停/继续功能
- [x] 进度条跟踪歌曲进度功能
- [x] 点击进度条设置播放进度功能
- [x] 日志文件和日志条系统

由于学业任务繁重，规划了但仍未实现的功能有

- [ ] 播放列表删除曲目
- [ ] 歌曲收藏功能
- [ ] 歌单收藏功能
- [ ] 创建本地歌单功能
- [ ] 歌单分享上传至服务器功能
- [ ] 本地歌单删除功能
- [ ] 搜索歌曲功能

收藏功能的数据库基础已经建立，但是由于时间仓促没能赶工收尾。完成这些功能预计还需要2周时间。

非常可惜建立本地歌单功能没能赶完，但是我们另外写了一款迷你本地播放器，
特别有意思，强烈推荐去看看，在[app.MiniPlayer](src/main/java/app/MiniPlayer.java)中，
在MiniPlayer类中测试使用。一定要看MiniPlayer！！！

## 使用指南

运行MusicPlayer，等待初始化界面加载完成，可以看到窗口。壁纸每3秒切换一张。
右上方有最小化、最大化、关闭按钮。拖动顶部面板可以移动窗口。
点击左上方“我的音乐”会弹出内部窗口，有三个标签导航“音乐广场”，“本地下载”，“我的收藏”。
在音乐广场中，如果是第一次打开，歌单需要加载。点击刷新按钮，会向服务器请求所有歌单并加载出来。
再次打开MusicPlayer后会向本地数据库中读取信息。

点击某一歌单可以看到歌单详情页面，详情页面有四个按钮，“返回”、“加载”、“收藏歌单”（暂未实现），“播放歌单”
点击返回会回到音乐广场主页面，点击加载会发起新线程向服务器下载音乐，并加载出来成列表。
点击播放歌单会将当前播放列表替换为该歌单的所有歌曲。必须线加载才能播放歌单。
双击歌曲列表中的每一首歌，会将这首歌加入播放列表之首并立即播放；如果播放列表中已有该歌曲，
则会将播放列表会直接跳到对应的歌曲并立即开始播放。

回到音乐广场主页面，点击“所有歌曲”会来到一个类似于歌单详情的页面，点击“加载”会建立进程下载所有歌曲。
同样地，点击“播放全部”会将播放列表替换为所有歌曲。

左侧的播放列表，绿色的表示正在播放的歌曲，播放结束后会立即切换下一首歌。双击某一个列表会立即切换并播放这首歌。
播放列表的歌曲支持拖动，拖动到特定位置松开鼠标，歌曲的播放顺序就会被调至该位置。

下方的控制面板，有“上一首”“下一首”“暂停/播放”的控制按钮，以及一个进度条，点击进度条可以立即跳转进度并继续播放。

## 架构设计

包图如下。app是程序入口，core是播放和控制的核心实现，可以移植，dao是数据库部分，
online是与服务器交互的部分，player.gui是界面绘制和player.model是数据模型。

![img.png](package.png)

用例图如下。用户可以在我的音乐中唤出音乐广场等等功能窗，可以实现查看所有歌单、查看歌单中的所有音乐、
查看所有音乐、重设播放列表、加入播放列表的功能，其中的歌单和歌曲信息会先从本地数据库加载，也可以在音乐广场功能窗下
点击刷新从服务器中获取。
播放列表可以通过拖动歌曲来调换播放顺序。
底部的控制面板中可以控制上一首、下一首、暂停、继续和点击进度条跳转进度。
![img.png](usecase.png)

## 核心实现

mp3音乐文件解码的功能依赖于JLayer库的AdvancePlayer，
其本身是有一个进度Listener的，但是事实证明它的Listener是不能使用的，于是我们
自己实现了对进度的Listener，精确跟踪解码器的解码进度。这个core包的内容是可以直接移植使用的。

```java
package core;

public class ProcessListener {
    private int frame;

    public int getFrame() {
        return frame;
    }

    public void setFrame(int frame) {
        this.frame = frame;
    }

    public void nextFrame() {
        frame++;
    }

    public void rewind() {
        frame = 0;
    }
}

```

重写AdvancePlayer中的解码函数（幸好它是个保护类型而不是私有类型，可以改写）
逐帧跟踪进度

```java    
package core;

public class MP3Player extends AdvancedPlayer {
    //...
    @Override
    protected boolean decodeFrame() {
        boolean ret = super.decodeFrame();
        process.nextFrame();    //在此加入逐帧跟踪
        return ret;
    }
}

```

每首歌只能获取其歌曲时长信息，因此想要知道当前进度百分比的话还需要获得歌曲总帧数。
AdvancePlayer的优越之处在于其拥有skipFrame方法，迅速跳过帧但不播放。这就相当于随机读写。

```java
package core;

public class MP3Player extends AdvancedPlayer {
    //...
    @Override
    private MP3Player(InputStream musicStream) {
        super(musicStream);
        boolean ret = true;
        int cur = 0;
        while (ret) {
            ret = skipFrame(); //跳过所有帧，并且记录总帧数
            ++cur;
        }
        totalFrame = cur;
    }
}
```

由于MP3Player播放时，会发生阻塞，因此需要调用线程来执行这项任务。
MP3Controller 是这个些任务的总监管，当需要播放时，就新建一个线程，从而建立一个播放器（解码器）对象，
边解码边播放音乐，当暂停发生时，强行关闭player，从而线程也自然走向结束，播放进度由Listener一直保留下来。
当想要继续时，想要恢复此前的进度，只需要让player跳过此前记录的帧数，然后正常播放就可以了。
而想要切换音乐而不是继续播放时，则需要rewind，让Listener跟踪重新记录帧数。

```java
package core;

import java.io.*;

public class MP3Controller {
    //...
    public void pause() {
        if (player != null) {
            player.close(); //关闭player，让播放线程终止
            player = null;
            playingSong = null;
        }
    }

    public void continues() {
        if (!running()) {
            playingSong = backup;
            playThread = new PlayThread();//新建播放线程
            playThread.start();
        }
    }

    public void switchMusic(File mp3File) {
        pause();
        backup = mp3File;
        process.rewind();//切换歌曲并重计进度
        continues();
    }

    public void gotoProcess(int frame) {
        if (frame < 0) frame = 0;
        if (frame > totalFrame) frame = totalFrame;
        pause();
        process.setFrame(frame);
//        continues();
    }

}


```

在此基础上跳转进度也就易如反掌了，只需捕捉进度条上的双击事件，计算鼠标坐标对应的进度
百分比值，然后计算对应的歌曲帧数，停止播放并迅速新建新的播放线程跳过的对应帧数恢复播放就可以了，
整个过程非常快，几乎零间隙。

## 数据库设计

dao包里是对Music和Sheet模型的增删改查原子API的定义。数据库里放置了四个表，歌曲、歌单、收藏歌曲、收藏歌单。
收藏歌单和收藏歌曲里只记录了歌曲和歌单在表中的id。
以下是数据库截图：

![database.png](screenshots/database.png)

![database_music.png](screenshots/database_music.png)

![database_sheet.png](screenshots/database_sheet.png)

![database_favorite_music.png](screenshots/database_favorite_music.png)

![database_favorite_sheet.png](screenshots/database_favorite_sheet.png)

## 任务分配

我们两个人在一起线下讨论的时间非常长，大部分工作是一起完成的，但是每个人还是提出了不同的创新点。介绍一下最核心的贡献。

罗浩：

* 研究明白AdvancePlayer的机制，设计出core包下的核心部分，精准捕捉和控制音乐播放进度。
* 设计了日志系统。
* 测试发现和修复了一些IO一致性问题。解决了一些线程阻塞问题，用子线程来完成一些高耗时任务，例如请求服务器下载文件、请求数据库等。
* 在开发过程中遇到了瓶颈时，重新梳理调整了项目结构，解决了组件之间的通信问题。
* 提出美术风格一致性，采用LIGHT_GRAY - GRAY - DARK_GRAY 简约主题，设计ScrollBarUI。

李兴男：

* 提出主窗口top、center、bottom三个部分的总体布局。
* 提出标签导航的人机交互思路，使界面简洁易用。
* 调通本地数据库dao部分，以及请求服务器online部分功能。
* 提出自动切换壁纸图片的美术想法，提供ui美术资源，以及播放列表上的半透明小呆图像。

从工作量上，我们一致认为 罗浩：李兴男 = 6:4

## 项目截图：

![main.png](screenshots/main.png)

![music_playing.png](screenshots/music_playing.png)

![](screenshots/img.png)

![sheet_detail.png](screenshots/sheet_detail.png)

![init](screenshots/init.png)

![logfile](screenshots/log_file.png)

![mp3Test](screenshots/mp3test.png)

![](screenshots/playlist_test.png)

2023/5/18更新
大进展，即将完工。可以播放歌单和全部歌曲了。
点击进度条可以跳转进度。支持暂停继续

2023/5/16更新
进一步优化日志，MP3Test中调通了设置进度功能。
优化小呆的出场，歌单可以添加进入播放列表了。

2023/5/15更新
添加日志文件，修复了部分加载时的bug，新增歌单细节页面

2023/5/13更新
全面优化日志结构，修复了一些重新加载歌单的bug，新增初始化画面

2023/5/11更新
优化了项目结构，添加了JInternalFrame功能

2023/5/9更新
添加播放列表功能，支持播放列表中的歌曲顺序改变

2023/5/8更新
目前进度：初步完成了Sqlite数据库部分。

2023/4/24更新
目前的进度：完成了GUI部分，位于[GUI部分](src/main/java/app)
完成了歌曲播放测试，位于[歌曲播放测试](src/main/java/app/MiniPlayer.java)
