package app;

import core.MP3Controller;
import player.gui.MainWindow;
import dao.SqliteUtil;
import player.model.PlayList;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

/**
 * 音乐播放器运行入口类
 *
 * @author 罗浩、李兴男
 */
public class MusicPlayer {

    /**
     * 日志文件
     */
    private final static File logFile = new File("musicplayer.log");
    /**
     * 日志标签
     */
    private final static JLabel log = new JLabel("log");
    /**
     * 初始化界面窗口
     */
    private final static JFrame preFrame = new JFrame();
    /**
     * 是否有日志文件。如果日志文件创建失败则设为false
     */
    private static boolean hasLogFile = true;

    /**
     * 播放列表。这是一个全局共享资源。
     *
     * @see PlayList
     */
    public static final PlayList playList = new PlayList();
    /**
     * 播放控制器。全局控制音乐播放。这是音乐播放器的核心。
     *
     * @see MP3Controller
     */
    public static final MP3Controller controller = new MP3Controller();

    static {
        try {
            if (!logFile.exists()) {
                hasLogFile = logFile.createNewFile();   //如果日志文件重新创建失败，则设置hasLogFile为false
            }
            FileWriter writer = new FileWriter(logFile);
            writer.write(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\n");
            writer.close();
        } catch (IOException e) {
            log("log file is unable to create");
        }

        log.setFont(new Font("微软雅黑", Font.PLAIN, 10));  //美化log标签
        log.setOpaque(true);
        log.setBackground(Color.DARK_GRAY);
        log.setForeground(Color.LIGHT_GRAY);
    }

    /**
     * 记录新的日志。更新log标签的内容，以及当日志文件存在时向日志文件追加日志信息
     *
     * @param log 日志内容
     */
    public static void log(String log) {
        MusicPlayer.log.setText(log);
        System.err.println(log);
        if (hasLogFile) {
            try {
                FileWriter writer = new FileWriter(logFile, true);
                writer.write(log + '\n');
                writer.close();
            } catch (IOException e) {
                log("error while writing to log file " + Arrays.toString(e.getStackTrace()));
                hasLogFile = false;
            }
        }
    }

    /**
     * 入口函数。
     * 分为两个阶段，准备阶段和启动阶段，准备阶段会显示初始化界面，然后直到主窗口出现初始化界面初始化界面就dispose。
     *
     * @param args 不需要参数列表
     */
    public static void main(String[] args) {
        pre();
        start();
    }

    /**
     * 准备阶段。
     * 形成初始化界面。
     */
    private static void pre() {
        preFrame.setBounds(200, 200, 320, 200);
        preFrame.setLocationRelativeTo(null);
        preFrame.setUndecorated(true);
        preFrame.setOpacity(0.9f);
        JPanel panel = new JPanel(new BorderLayout()) {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Image fluttershy;
                try {
                    BufferedImage fluttershy_buffer = ImageIO.read(new File("art/componentsPicture/listening_flutter.jpg"));
                    ImageProducer fluttershy_source = new FilteredImageSource(fluttershy_buffer.getSource(), new RGBImageFilter() {
                        @Override
                        public int filterRGB(int x, int y, int rgb) {
                            if ((rgb & 0xff000000) == 0) return 0;
                            return (rgb & 0x00ffffff) | 0xcc000000;
                        }
                    });
                    fluttershy = Toolkit.getDefaultToolkit().createImage(fluttershy_source);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                g.drawImage(fluttershy, (getWidth() - getHeight() + 20) / 2, 0, getHeight() - 20, getHeight() - 20, preFrame);
            }
        };
        panel.setBackground(Color.GRAY);
        JLabel label = new JLabel("初始化中，请稍候...");
        label.setFont(new Font(null, Font.BOLD, 25));
        panel.add(label, BorderLayout.CENTER);
        panel.add(log, BorderLayout.SOUTH);
        preFrame.add(panel, BorderLayout.CENTER);
        preFrame.setVisible(true);
    }

    /**
     * 启动阶段。
     * 如果没有数据库则生成数据库。启动主窗口，当启动完毕后将初始化界面dispose，并将log标签传递给主窗口
     *
     * @see MainWindow
     * @see SqliteUtil
     */
    private static void start() {

        SqliteUtil.createTables();
        try {
            MainWindow mainWindow = new MainWindow();
            mainWindow.setVisible(true);
            mainWindow.setDefaultCloseOperation(MainWindow.EXIT_ON_CLOSE);
            preFrame.dispose();
            mainWindow.setLog(log);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
