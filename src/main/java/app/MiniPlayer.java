package app;

import online.util.SongFilter;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.id3.AbstractID3v2Frame;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import org.jaudiotagger.tag.id3.framebody.FrameBodyAPIC;
import core.MP3Controller;
import org.jetbrains.annotations.NotNull;
import player.model.Picture;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 迷你音乐播放器
 *
 * @author 罗浩、李兴男
 */
public class MiniPlayer {
    static final MP3Controller controller = new MP3Controller();
    static File song = null;
    /**
     * 歌曲的总时长
     */
    static int time = 0;
    static final JLabel songName = new JLabel("Unknown Song Name");
    static final JLabel singer = new JLabel("Unknown Singer");
    static Picture picture = new Picture(70);
    /**
     * 输入特定进度。需要格式化成数字
     */
    static final JFormattedTextField setToProcessTextField;
    static final JLabel status = new JLabel();

    /*
      输入的文字必须是数字。设置状态标签的颜色、位置
     */
    static {
        NumberFormat format = NumberFormat.getInstance();
        format.setGroupingUsed(false);
        setToProcessTextField = new JFormattedTextField(format);
        setToProcessTextField.setColumns(5);
        status.setText("快来试试设定进度功能吧！");
        status.setForeground(Color.BLUE);
        status.setAlignmentX(0.5f);
    }

    public static void main(String[] args) {
        String lookAndFeel = UIManager.getSystemLookAndFeelClassName();
        try {
            UIManager.setLookAndFeel(lookAndFeel);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException |
                 UnsupportedLookAndFeelException e) {
            throw new RuntimeException(e);
        }
        final Timer processPrinter = new Timer();
        JFrame jFrame = new JFrame("MiniPlayer") {
            @Override
            public void dispose() {
                super.dispose();
                processPrinter.cancel();    //停止这些线程
                controller.pause(); //controller务必要终止，否则关闭窗口后还会继续播放
            }
        };
        JLabel processStat = new JLabel();  //记录进度状态
        JLabel timeStat = new JLabel();     //记录时长状态
        JButton changeSong = new JButton("Change a Song");  //从磁盘中选歌
        JButton controlBtn = new JButton(); //暂停、继续
        JButton confirm = new JButton("confirm");
        JPanel infoPanel = new JPanel();    //右侧的信息面板
        JPanel controlPanel = new JPanel(); //放置按钮的控制面板
        JPanel centerPanel = new JPanel();
        JPanel setToProcessPanel = new JPanel();
        jFrame.setBounds(100, 100, 270, 200);
        jFrame.getContentPane().setLayout(new BoxLayout(jFrame.getContentPane(), BoxLayout.Y_AXIS));
        jFrame.getContentPane().add(centerPanel);
        jFrame.getContentPane().add(setToProcessPanel);
        jFrame.getContentPane().add(status);
        jFrame.add(controlPanel, BorderLayout.SOUTH);
        centerPanel.setBorder(BorderFactory.createEtchedBorder());
        centerPanel.add(picture);
        centerPanel.add(infoPanel);
        setToProcessPanel.add(new JLabel("set process here"));
        setToProcessPanel.add(setToProcessTextField);
        setToProcessPanel.add(confirm);
        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
        infoPanel.add(songName);
        infoPanel.add(singer);
        infoPanel.add(timeStat);
        infoPanel.add(processStat);
        controlPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        controlPanel.add(controlBtn);
        controlPanel.add(changeSong);

//        switchSong(new File("music.sample/黄小琥 - 没那么简单.mp3"));

        processPrinter.schedule(new TimerTask() {
            /**
             * 每隔半秒刷新一次，更新界面信息
             */
            @Override
            public void run() {
                float process = (float) controller.getProcess() / controller.getTotalFrame();
                timeStat.setText("%02d:%02d / %02d:%02d".formatted((int) (time * process) / 60, (int) (time * process) % 60, time / 60, time % 60));
                processStat.setText("Music Process: " + "%%%.3f".formatted(100 * process));
                if (!controller.running()) {
                    controlBtn.setText("continue");
                } else {
                    controlBtn.setText("pause");
                }
            }
        }, 500, 500);
        controlBtn.setText("pause");
        controlBtn.addActionListener(controlListener);
        changeSong.addActionListener(changeSongListener);
        setToProcessTextField.addActionListener(setToProcess);
        confirm.addActionListener(setToProcess);


        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * 从mp3文件中获取艺术家、歌名、图片信息
     *
     * @param file mp3文件
     */
    static void trackInfo(File file) {
        try {
            MP3File mp3File = (MP3File) AudioFileIO.read(file);
            time = mp3File.getMP3AudioHeader().getTrackLength();
            AbstractID3v2Tag tag = mp3File.getID3v2Tag();
            if (tag != null) {
                AbstractID3v2Frame frame = (AbstractID3v2Frame) mp3File.getID3v2Tag().getFrame("APIC");
                FrameBodyAPIC body = (FrameBodyAPIC) frame.getBody();
                byte[] imageData = body.getImageData();
                singer.setText(tag.getFirst(FieldKey.ARTIST));
                songName.setText(tag.getFirst(FieldKey.TITLE));
                picture.setImage(Toolkit.getDefaultToolkit().createImage(imageData, 0, imageData.length));
                picture.repaint();
            } else {
                singer.setText("Unknown Singer");
                picture.setImage(Picture.DEFAULT);
                picture.repaint();
            }
        } catch (CannotReadException | InvalidAudioFrameException | ReadOnlyFileException | TagException |
                 IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 切歌
     *
     * @param newSong 选择将要播放的歌曲
     */
    static void switchSong(@NotNull File newSong) {
        if (newSong.exists()) {
            song = newSong;
            String fileName = song.getName();
            songName.setText(fileName.substring(0, fileName.lastIndexOf('.')));
            trackInfo(song);
            controller.switchMusic(song);
        }
    }

    /**
     * 设置到特定进度
     */
    static ActionListener setToProcess = (ActionEvent e) -> {
        float value = Float.parseFloat(setToProcessTextField.getText());
        if (value >= 0 && value < 100) {
            controller.gotoProcess((int) (value * controller.getTotalFrame() / 100));
            controller.continues();
            status.setForeground(Color.GREEN);
            status.setText("SUCCESS");
        } else {
            status.setForeground(Color.RED);
            status.setText("this is required to between 0 and 99");
        }
    };
    /**
     * 调出一个fileChooser，从磁盘中读取文件
     */
    static ActionListener changeSongListener = (ActionEvent e) -> {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.addChoosableFileFilter(SongFilter.filter);
        fileChooser.setFileHidingEnabled(true);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setCurrentDirectory(new File("music.sample"));
        fileChooser.setSelectedFile(song);
        fileChooser.showDialog(new JLabel(), "选择歌曲");
        File gotFile = fileChooser.getSelectedFile();
        if (gotFile != null && (song == null || !song.getName().equals(gotFile.getName()))) {
            switchSong(fileChooser.getSelectedFile());
        }
    };
    /**
     * 暂停和播放按钮事件
     */
    static ActionListener controlListener = e -> {
        JButton source = (JButton) e.getSource();
        switch (source.getText()) {
            case "pause" -> {
                controller.pause();
                source.setText("continue");
            }
            case "continue" -> {
                controller.continues();
                source.setText("pause");
            }
        }
    };

}
