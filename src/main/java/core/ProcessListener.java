package core;

/**
 * Track real-time playback progress.
 *
 * @author LuoHao
 */
public class ProcessListener {
    /**
     * records the progress
     */
    private int frame;

    /**
     * Property of frame
     *
     * @return real-time frame
     */
    public int getFrame() {
        return frame;
    }

    /**
     * set the frame
     *
     * @param frame new value of frame
     */
    public void setFrame(int frame) {
        this.frame = frame;
    }

    /**
     * increase the frame
     */
    public void nextFrame() {
        frame++;
    }

    /**
     * set frame to 0
     */
    public void rewind() {
        frame = 0;
    }
}
