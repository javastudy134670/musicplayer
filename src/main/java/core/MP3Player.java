package core;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.AdvancedPlayer;

import java.io.*;

/**
 * An improvement on AdvancePlayer
 *
 * @author LuoHao
 * @see AdvancedPlayer
 */
public class MP3Player extends AdvancedPlayer {
    /**
     * total frame
     */
    private final int totalFrame;
    /**
     * process listener
     * Track real-time playback progress.
     *
     * @see ProcessListener
     */
    private ProcessListener process;

    /**
     * This function is called to get the total frame of this music file.
     *
     * @param musicStream music file stream, the total frame of which will be recorded
     * @throws JavaLayerException from skipFrame()
     */
    private MP3Player(InputStream musicStream) throws JavaLayerException {
        super(musicStream);
        boolean ret = true;
        int cur = 0;
        while (ret) {
            ret = skipFrame();
            ++cur;
        }
        totalFrame = cur;
    }

    /**
     * Start or continue to play music.
     *
     * @param mp3file mp3 song file
     * @param process ProcessListener
     * @throws JavaLayerException from super
     * @throws IOException        from super
     * @see ProcessListener
     */
    public MP3Player(File mp3file, ProcessListener process) throws JavaLayerException, IOException {
        super(new FileInputStream(mp3file));
        MP3Player tmp = new MP3Player(new FileInputStream(mp3file));
        totalFrame = tmp.getTotalFrame();
        this.process = process;
        tmp.close();
    }

    /**
     * Property of totalFrame
     *
     * @return totalFrame
     */
    public int getTotalFrame() {
        return totalFrame;
    }

    /**
     * Playing mp3 song for one single frame
     *
     * @return true if there are no more frames to decode, false otherwise.
     * @throws JavaLayerException from super
     */
    @Override
    protected boolean decodeFrame() throws JavaLayerException {
        boolean ret = super.decodeFrame();
        process.nextFrame();
        return ret;
    }

}