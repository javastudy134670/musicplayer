/**
 * Core part of MusicPlayer.
 * This package is portable.
 * Realized those functions: switch a song, pause, continue, go to a process, got total frame.
 *
 * @auther LuoHao, LiXingnan
 */
package core;

import javazoom.jl.decoder.JavaLayerException;

import java.io.*;

/**
 * A robust manager to controls the playback of songs.
 * If an instance of this class is created, remember to call {@code  pause()} before it is abandoned.
 *
 * @author LuoHao
 */
public class MP3Controller {
    public MP3Controller() {
        player = null;
        playingSong = null;
        backup = null;
        playThread = null;
        totalFrame = 10;
        process = new ProcessListener();
    }

    /**
     * A single case to play a piece of music, which can be just a little small piece of it.
     * This reference can be redefined, witch means a new piece of sounds is going to be played.
     *
     * @see MP3Player
     */
    private MP3Player player;
    /**
     * A reference to a playing song file, which needs to be defined before a new {@code player} is about to play.
     */
    private File playingSong;
    /**
     * This file record the song file played last time. This helps restore song playback.
     */
    private File backup;
    /**
     * Total frame.
     */
    private int totalFrame;
    /**
     * Instantly record the progress of music playback.
     *
     * @see ProcessListener
     */
    private final ProcessListener process;
    /**
     * Due to the blocking nature of class {@code  MP3Player}, a new thread is required for each playback.
     *
     * @see PlayThread
     */
    private PlayThread playThread;
    /**
     * The status whether the player's music playing completed or not.
     */
    private boolean over = false;

    /**
     * @return {@code true} if MP3Player is playing, otherwise {@code  false}.
     */
    public boolean running() {
        return playingSong != null;
    }

    /**
     * The status whether the player's music playing completed or not.
     *
     * @return true if is completed
     */
    public boolean isOver() {
        return over;
    }

    /**
     * Shut up the playing {@code player}. The process is progress will be preserved by {@code ProcessListener}.
     *
     * @see ProcessListener
     */
    public void pause() {
        if (player != null) {
            player.close();
            player = null;
            playingSong = null;
        }
    }

    /**
     * Restore the playing song, and continue to play.
     *
     * @see PlayThread
     */
    public void continues() {
        if (!running()) {
            playingSong = backup;
            playThread = new PlayThread();
            playThread.start();
        }
    }

    /**
     * Choose a song File to play.
     *
     * @param mp3File the song file to be played
     */
    public void switchMusic(File mp3File) {
        pause();
        backup = mp3File;
        process.rewind();
        continues();
    }

    /**
     * Pause and set the progress of playback.
     * frame is asked to be not less than 0, and not bigger than {@code  totalFrame}
     *
     * @param frame the song progress will be adjusted to it.
     */
    public void gotoProcess(int frame) {
        if (frame < 0) frame = 0;
        if (frame > totalFrame) frame = totalFrame;
        pause();
        process.setFrame(frame);
//        continues();
    }

    /**
     * Property of totalFrame.
     *
     * @return totalFrame
     */
    public int getTotalFrame() {
        return totalFrame;
    }

    /**
     * Track real-time playback progress
     *
     * @return the progress frame
     * @see ProcessListener
     */
    public int getProcess() {
        return process.getFrame();
    }

    /**
     * Due to the blocking nature of class {@code  MP3Player}, a new thread is required for each playback.
     * After playing, a pause instruction is required, to shut up the player.
     */
    private class PlayThread extends Thread {
        @Override
        public void run() {
            if (running()) {
                try {
                    over = false;
                    player = new MP3Player(playingSong, process);
                    totalFrame = player.getTotalFrame();
                    player.play(process.getFrame(), totalFrame);
                    over = true;
                    pause();
                } catch (IOException | JavaLayerException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

