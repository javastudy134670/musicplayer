package online.operation;


import app.MusicPlayer;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;

public class FileDownloader {
    private static final int SUCCESS = 200;

    public static int downloadMusicFile(String url, String fileMd5, String targetPath) {
        return downloadMusicFile(url, fileMd5, targetPath, null);
    }

    public static int downloadMusicFile(String url, String fileMd5, String targetPath, String fileName) {
        HttpClient client = new HttpClient();
        GetMethod get = null;
        FileOutputStream output = null;
        String filename = fileName;

        int i = 404;
        try {
            get = new GetMethod(url + "?md5=" + fileMd5);
            if (InetAddress.getByName("119.167.221.16").isReachable(1000)) {
                i = client.executeMethod(get);
            }
            if (SUCCESS == i) {
                if (fileName == null) {
                    filename = java.net.URLDecoder
                            .decode(get.getResponseHeader("Content-Disposition").getValue().substring(21), StandardCharsets.UTF_8);
                }
                File pathDirectory = new File(targetPath);
                if (!pathDirectory.exists()) {
                    if (!pathDirectory.mkdirs()) {
                        throw new RuntimeException("make dir fail");
                    }
                }
                MusicPlayer.log("downloading music: %s ...%n".formatted(filename));
                File storeFile = new File(targetPath + "/" + filename);
                InputStream inputStream = get.getResponseBodyAsStream();
                output = new FileOutputStream(storeFile);
                byte[] buf = new byte[1024];
                int len;
                while ((len = inputStream.read(buf)) > 0) {
                    output.write(buf, 0, len);
                }
            } else {
                MusicPlayer.log("DownLoad file failed with error code: " + i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            assert get != null;
            get.releaseConnection();
            client.getHttpConnectionManager().closeIdleConnections(0);
        }
        return i;
    }

    public static int downloadMusicSheetPicture(String url, String musicSheetUuid, String targetPath) {
        HttpClient client = new HttpClient();
        GetMethod get = null;
        FileOutputStream output = null;
        String filename = null;

        int i = 404;
        try {
            get = new GetMethod(url + "?uuid=" + musicSheetUuid);
            if (InetAddress.getByName("119.167.221.16").isReachable(1000)) {
                i = client.executeMethod(get);
            }

            if (SUCCESS == i) {

                var responseHeader = get.getResponseHeader("Content-Disposition");
                if (responseHeader != null) {
                    filename = java.net.URLDecoder
                            .decode(get.getResponseHeader("Content-Disposition").getValue().substring(21), StandardCharsets.UTF_8);
                }
                File pathDirectory = new File(targetPath);
                if (!pathDirectory.exists()) {
                    if (!pathDirectory.mkdirs()) {
                        throw new RuntimeException("make dir fail");
                    }
                }
                MusicPlayer.log("downloading picture %s ...%n".formatted(filename));
                File storeFile = new File(targetPath + "/" + filename);
                InputStream inputStream = get.getResponseBodyAsStream();
                output = new FileOutputStream(storeFile);
                byte[] buf = new byte[1024];
                int len;
                while ((len = inputStream.read(buf)) > 0) {
                    output.write(buf, 0, len);
                }
            } else {
                MusicPlayer.log("DownLoad file failed with error code: " + i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            assert get != null;
            get.releaseConnection();
            client.getHttpConnectionManager().closeIdleConnections(0);
        }
        return i;
    }
}
