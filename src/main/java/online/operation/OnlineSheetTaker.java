package online.operation;

import app.MusicPlayer;
import online.model.OnlineSheet;
import online.util.SongFilter;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class OnlineSheetTaker {
    public OnlineSheetTaker() {
    }

    /**
     * querySheets 查询所有歌单
     *
     * @param url  向服务器请求取得所有歌单的服务器请求链接
     * @param port 请求端口
     * @return messages 返回歌单列表
     */
    public static HashMap<String, OnlineSheet> queryOnlineSheets(String url, int port) throws IOException {
        //向服务器发出请求
        HttpClient client = new HttpClient();
        client.getHostConfiguration().setHost(url, port);
        GetMethod method = new GetMethod(url);
        client.executeMethod(method);

        MusicPlayer.log("while queryOnlineSheets ...");
        //打印服务器返回状态
        MusicPlayer.log(method.getStatusLine().toString());

        //获得服务器中得来的musicSheetList表项，将每一条上传记录形成JSONArray
        InputStream bodyStream = method.getResponseBodyAsStream();

        JSONObject jsonObject = new JSONObject(convertStreamToString(bodyStream));
        JSONArray jsonSheetList = (JSONArray) jsonObject.get("musicSheetList");

        HashMap<String, OnlineSheet> hashMap = new HashMap<>();
        for (var musicSheetObj : jsonSheetList) {
            JSONObject jsonMessage = new JSONObject(musicSheetObj.toString());
            OnlineSheet onlineSheet = new OnlineSheet();
            onlineSheet.setUuid((String) jsonMessage.get("uuid"));
            onlineSheet.setName((String) jsonMessage.get("name"));
            onlineSheet.setCreator((String) jsonMessage.get("creator"));
            onlineSheet.setCreatorId((String) jsonMessage.get("creatorId"));
            onlineSheet.setDateCreated(((String) jsonMessage.get("dateCreated")).substring(0, 10));
            onlineSheet.setPicture((String) jsonMessage.get("picture"));
            JSONObject jsonMusicItems = jsonMessage.getJSONObject("musicItems");
            var musicItemsMap = new HashMap<String, String>();
            for (var key : jsonMusicItems.keySet()) {
                String song = jsonMusicItems.getString(key);
                if (SongFilter.accept(new File(song)))
                    musicItemsMap.put(key, jsonMusicItems.getString(key));
            }
            if (musicItemsMap.isEmpty() || onlineSheet.getName().isEmpty()) continue;
            onlineSheet.setMusicItems(musicItemsMap);
            if (hashMap.containsKey(onlineSheet.getName())) {
                onlineSheet.getMusicItems().putAll(hashMap.get(onlineSheet.getName()).getMusicItems());
            }
            hashMap.put(onlineSheet.getName(), onlineSheet);
        }

        return hashMap;
    }

    private static String convertStreamToString(InputStream bodyStream) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(bodyStream, StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bodyStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
