package online.util;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class SongFilter {
    public final static FileNameExtensionFilter filter = new FileNameExtensionFilter("*.mp3", "mp3");

    public static boolean accept(File file) {
        return filter.accept(file);
    }
}
