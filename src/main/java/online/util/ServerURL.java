package online.util;

public class ServerURL {
    public static final String URL_Server = "http://119.167.221.16:38080/music.server";
    public static final String URL_DownloadMusicFile = URL_Server + "/downloadMusic";
    public static final String URL_DownloadMusicSheetPicture = URL_Server + "/downloadPicture";
    public static final String URL_CreateMusicSheetAndUploadFiles = URL_Server + "/upload";
    public static final String URL_QueryAllMusicSheets = URL_Server + "/queryMusicSheets?type=all";
}
