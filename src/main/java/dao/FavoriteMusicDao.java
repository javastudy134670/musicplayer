package dao;

import player.model.Music;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FavoriteMusicDao implements BaseDao<Music> {
    @Override
    public void insert(Music music) {
        String sql = "INSERT INTO FAVORITE_MUSIC(MUSIC_ID)VALUES (?)";
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = SqliteUtil.getConnection();
            ps = connection.prepareStatement(sql);
            ps.setInt(1, music.id());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            SqliteUtil.close(null, ps, connection);
        }
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM FAVORITE_MUSIC WHERE MUSIC_ID = ?";
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = SqliteUtil.getConnection();
            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            SqliteUtil.close(null, ps, connection);
        }
    }

    @Override
    @Deprecated
    public void update(Music music) throws SQLException {
        throw new SQLException("this function is not suppose to use");
    }

    @Override
    public List<Music> findAll() {
        List<Music> list = new ArrayList<>();
        String sql = "SELECT MUSIC_ID FROM FAVORITE_MUSIC";
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = SqliteUtil.getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new MusicDao().findById(rs.getInt(1)));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            SqliteUtil.close(rs, ps, connection);
        }
        return list;
    }

    @Override
    public Music findById(int id) {
        return new MusicDao().findById(id);
    }

    @Override
    public int getId(String MD5_or_UUID) {
        return new MusicDao().getId(MD5_or_UUID);
    }

}
