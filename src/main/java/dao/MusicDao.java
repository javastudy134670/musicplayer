package dao;

import app.MusicPlayer;
import player.model.Music;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 歌曲数据操作类
 */
public class MusicDao implements BaseDao<Music> {

    @Override
    public void insert(Music t) {
        String sql = "INSERT INTO MUSIC (NAME, SHEET_ID, FILE_PATH, MD5) VALUES (?, ?, ?, ?)";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = SqliteUtil.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, t.name());
            ps.setInt(2, t.sheetId());
            ps.setString(3, t.filePath());
            ps.setString(4, t.md5());
            ps.executeUpdate();
            MusicPlayer.log("[INSERT]: " + ps);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SqliteUtil.close(null, ps, conn);
        }
    }

    @Override
    public void delete(int id) {
        // TODO Auto-generated method stub
        String sql = "DELETE FROM MUSIC WHERE id=?";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = SqliteUtil.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            MusicPlayer.log("[DELETE]: " + ps);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SqliteUtil.close(null, ps, conn);
        }
    }

    @Override
    public void update(Music t) {
        String sql = "UPDATE MUSIC SET NAME=?, SHEET_ID=?, FILE_PATH=?, MD5=? WHERE ID=?";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = SqliteUtil.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, t.name());
            ps.setInt(2, t.sheetId());
            ps.setString(3, t.filePath());
            ps.setString(4, t.md5());
            ps.setInt(5, t.id());
            ps.executeUpdate();
            MusicPlayer.log("[UPDATE]: " + ps);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SqliteUtil.close(null, ps, conn);
        }

    }

    @Override
    public List<Music> findAll() {
        // TODO Auto-generated method stub
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Music> musics = new ArrayList<>();
        String sql = "SELECT NAME, SHEET_ID, FILE_PATH, MD5, ID FROM MUSIC";
        conn = SqliteUtil.getConnection();
        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Music music = new Music(
                        rs.getInt(5),
                        rs.getString(1),
                        rs.getInt(2),
                        rs.getString(4),
                        rs.getString(3)
                );
                musics.add(music);
            }
            MusicPlayer.log("[FIND]: " + ps);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SqliteUtil.close(rs, ps, conn);
        }
        return musics;
    }

    @Override
    public Music findById(int id) {
        // TODO Auto-generated method stub
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Music music = null;
        String sql = "SELECT NAME, SHEET_ID, FILE_PATH, MD5, ID FROM MUSIC WHERE ID=?";
        try {
            conn = SqliteUtil.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                music = new Music(
                        id,
                        rs.getString(1),
                        rs.getInt(2),
                        rs.getString(4),
                        rs.getString(3)
                );
            }
            MusicPlayer.log("[FIND]: " + ps);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SqliteUtil.close(rs, ps, conn);
        }
        return music;
    }

    /**
     * 基于MD5查询ID
     *
     * @param MD5 MD5值
     * @return 音乐ID
     */
    @Override
    public int getId(String MD5) {
        List<Music> musics = findAll();
        for (var music : musics) {
            if (music.md5().equals(MD5)) {
                return music.id();
            }
        }
        return -1;
    }

    /**
     * 基于歌单ID查询所有属于该歌单的歌曲
     *
     * @param sheetId 歌单ID
     * @return musics 该歌单下的所有歌曲
     */
    public List<Music> findBySheetId(int sheetId) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Music> musics = new ArrayList<>();
        String sql = "SELECT NAME, SHEET_ID, FILE_PATH, MD5, ID FROM MUSIC WHERE SHEET_ID=?";
        try {
            conn = SqliteUtil.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, sheetId);
            rs = ps.executeQuery();
            while (rs.next()) {
                Music music = new Music(
                        rs.getInt(5),
                        rs.getString(1),
                        rs.getInt(2),
                        rs.getString(4),
                        rs.getString(3)
                );
                musics.add(music);
            }
            MusicPlayer.log("[FIND]: " + ps);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SqliteUtil.close(rs, ps, conn);
        }
        return musics;
    }

}