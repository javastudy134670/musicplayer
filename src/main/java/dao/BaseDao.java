package dao;

import java.sql.SQLException;
import java.util.List;

/**
 * 数据表操作基础接口
 *
 * @param <T>
 */
public interface BaseDao<T> {
    /**
     * 增
     */
    void insert(T t);

    /**
     * 删
     */
    void delete(int id);

    /**
     * 改
     */
    void update(T t) throws SQLException;

    /**
     * 查（查所有）
     */
    List<T> findAll();

    /**
     * 查（基于ID查单条）
     */
    T findById(int id);

    /**
     * 获得ID
     *
     * @return ID
     */
    int getId(String MD5_or_UUID) throws SQLException;
}
