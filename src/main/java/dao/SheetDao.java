package dao;


import app.MusicPlayer;
import player.model.Sheet;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 操作歌单表的数据操作对象类
 */
public class SheetDao implements BaseDao<Sheet> {
    /**
     * 向歌单表中插入新歌单记录
     */
    @Override
    public void insert(Sheet sheet) {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "INSERT INTO SHEET (NAME, DATE_CREATED, CREATOR, PIC_PATH, UUID) VALUES (?, ?, ?, ?, ?)";
        try {
            conn = SqliteUtil.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, sheet.name());
            ps.setString(2, sheet.dateCreated());
            ps.setString(3, sheet.creator());
            ps.setString(4, sheet.picPath());
            ps.setString(5, sheet.uuid());
            ps.executeUpdate();
            MusicPlayer.log("[INSERT]: insert into sheet");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SqliteUtil.close(null, ps, conn);
        }
    }

    /**
     * 删除歌单（基于ID删除）
     */
    @Override
    public void delete(int id) {
        String sql = "DELETE FROM SHEET WHERE ID=?";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = SqliteUtil.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            MusicPlayer.log("[DELETE]: " + ps);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SqliteUtil.close(null, ps, conn);
        }
    }

    /**
     * 修改歌单
     */
    @Override
    public void update(Sheet sheet) {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql = "UPDATE SHEET SET NAME=?, DATE_CREATED=?, CREATOR=?, PIC_PATH=?, UUID=? WHERE ID=?";
        try {
            conn = SqliteUtil.getConnection();
            ps = conn.prepareStatement(sql);

            ps.setString(1, sheet.name());
            ps.setString(2, sheet.dateCreated());
            ps.setString(3, sheet.creator());
            ps.setString(4, sheet.picPath());
            ps.setString(5, sheet.uuid());
            ps.setInt(6, sheet.id());
            ps.executeUpdate();
            MusicPlayer.log("[UPDATE]: " + ps);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SqliteUtil.close(null, ps, conn);
        }

    }

    /**
     * 列出所有歌单
     */
    @Override
    public List<Sheet> findAll() {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Sheet sheet;
        List<Sheet> sheets = new ArrayList<>();
        String sql = "SELECT ID, NAME, DATE_CREATED, CREATOR, PIC_PATH, UUID FROM SHEET";
        try {
            conn = SqliteUtil.getConnection();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                sheet = getSheet(rs);
                sheets.add(sheet);
            }
            MusicPlayer.log("[FIND]: " + ps);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SqliteUtil.close(rs, ps, conn);
        }
        return sheets;
    }

    @NotNull
    private Sheet getSheet(ResultSet rs) throws SQLException {
        Sheet sheet;
        sheet = new Sheet(
                rs.getInt(1),
                rs.getString(6),
                rs.getString(2),
                rs.getString(4),
                rs.getString(3),
                rs.getString(5)
        );
        return sheet;
    }

    /**
     * 基于歌单ID查询歌单（只有一个，因为歌单ID是唯一的主键）
     */
    @Override
    public Sheet findById(int id) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Sheet sheet = null;
        String sql = "SELECT ID, NAME, DATE_CREATED, CREATOR, PIC_PATH, UUID FROM SHEET WHERE ID=?";
        try {
            conn = SqliteUtil.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                sheet = getSheet(rs);
            }
            MusicPlayer.log("[FIND]: " + ps);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SqliteUtil.close(rs, ps, conn);
        }
        return sheet;
    }

    @Override
    public int getId(String UUID) {
        List<Sheet> sheets = findAll();
        for (var sheet : sheets) {
            if (sheet.uuid().equals(UUID)) {
                return sheet.id();
            }
        }
        return -1;
    }
}