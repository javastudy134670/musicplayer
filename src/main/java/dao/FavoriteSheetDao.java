package dao;

import player.model.Sheet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FavoriteSheetDao implements BaseDao<Sheet> {
    @Override
    public void insert(Sheet sheet) {
        String sql = "INSERT INTO FAVORITE_SHEET(SHEET_ID)VALUES (?)";
        Connection connection = SqliteUtil.getConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, sheet.id());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            SqliteUtil.close(null, ps, connection);
        }
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM FAVORITE_SHEET WHERE SHEET_ID = ?";
        Connection connection = SqliteUtil.getConnection();
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            SqliteUtil.close(null, ps, connection);
        }
    }

    @Override
    @Deprecated
    public void update(Sheet sheet) throws SQLException {
        throw new SQLException("this function is not suppose to use");
    }

    @Override
    public List<Sheet> findAll() {
        List<Sheet> list = new ArrayList<>();
        String sql = "SELECT SHEET_ID FROM FAVORITE_SHEET";
        Connection connection = SqliteUtil.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new SheetDao().findById(rs.getInt(1)));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            SqliteUtil.close(rs, ps, connection);
        }
        return list;
    }

    @Override
    public Sheet findById(int id) {
        return new SheetDao().findById(id);
    }

    @Override
    public int getId(String MD5_or_UUID) {
        return new SheetDao().getId(MD5_or_UUID);
    }
}
