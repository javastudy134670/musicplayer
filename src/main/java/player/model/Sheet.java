package player.model;

/**
 *
 * @param id id
 * @param uuid uuid
 * @param name name
 * @param creator creator
 * @param dateCreated dateCreated
 * @param picPath picturePath
 */
public record Sheet(
        int id,
        String uuid,
        String name,
        String creator,
        String dateCreated,
        String picPath
) {}
