package player.model;

public record Music(
        int id,
        String name,
        int sheetId,
        String md5,
        String filePath
) {
    @Override
    public String toString() {
        return name();
    }
}