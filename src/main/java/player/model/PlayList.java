package player.model;

import app.MusicPlayer;
import player.gui.bottom.BottomPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.File;
import java.io.IOException;
import java.util.Collection;

public class PlayList {

    private int cur = 0;
    private final DefaultListModel<Music> playListModel = new DefaultListModel<>();
    private final Image derpy;
    private final JList<Music> jList;

    public boolean isEmpty() {
        return playListModel.isEmpty();
    }

    public JList<Music> getjList() {
        return jList;
    }

    public void reset(Collection<Music> list) {
        playListModel.clear();
        playListModel.addAll(list);
        cur = 0;
    }

    public void addFront(Music music) {
        int index = playListModel.indexOf(music);
        if (index == -1) {
            playListModel.add(0, music);
            cur = 0;
        } else {
            cur = index;
        }
        BottomPanel.switchSong(currentMusic());
        MusicPlayer.controller.continues();
        jList.repaint();
    }

    public void addBack(Music music) {
        if (playListModel.contains(music)) {
            MusicPlayer.log("[WARN]: Add Songs Repeatedly.");
            return;
        }
        playListModel.addElement(music);
    }

    public Music currentMusic() {
        return playListModel.getElementAt(cur);
    }


    public void next() {
        cur = (cur + 1) % playListModel.size();
        jList.repaint();
    }

    public void last() {
        cur = (cur + playListModel.size() - 1) % playListModel.size();
        jList.repaint();
    }

    public PlayList() {
        MusicPlayer.log("While construct " + this.getClass().getName());

        jList = new JList<>(playListModel) {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                JViewport parent = (JViewport) getParent();
                int offset = parent.getViewPosition().y;
                int width = derpy.getWidth(this);
                int height = derpy.getHeight(this);
                int actualHeight = parent.getWidth() * height / width;
                g.drawImage(derpy, 0, parent.getHeight() - actualHeight + offset, parent.getWidth(), actualHeight, this);
            }

            @Override
            public String getToolTipText(MouseEvent e) {
                int index = locationToIndex(e.getPoint());
                if (index > -1) {
                    return playListModel.get(index).name();
                } else {
                    return null;
                }
            }
        };
        jList.setBackground(Color.GRAY);
        jList.setCellRenderer((list, value, index, isSelected, cellHasFocus) -> {
            JLabel jLabel = new JLabel(value.name());
            jLabel.setFont(new Font(null, Font.PLAIN, 10));
            jLabel.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
            jLabel.setBackground(Color.GRAY);
            jLabel.setForeground(Color.BLACK);
            if (isSelected) {
                jLabel.setBackground(Color.LIGHT_GRAY);
            }
            if (index == cur) {
                jLabel.setForeground(Color.GREEN);
            }
            jLabel.setOpaque(true);
            return jLabel;
        });
        jList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int index = jList.locationToIndex(e.getPoint());
                    if (index > -1) {
                        cur = index;
                        BottomPanel.switchSong(currentMusic());
                        MusicPlayer.controller.continues();
                        jList.repaint();
                    }
                }
            }
        });
        jList.setDragEnabled(true);
        jList.setDropMode(DropMode.INSERT);
        DataFlavor musicFlavor = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType + ";class=" + Music.class.getName(), "Music");
        jList.setTransferHandler(new TransferHandler() {
            int index; // 记录被拖动元素的索引

            @Override
            public int getSourceActions(JComponent comp) {
// 返回支持的操作类型
                return COPY_OR_MOVE;
            }

            @Override
            public Transferable createTransferable(JComponent comp) {
// 返回一个 Transferable 对象，用于封装被拖动的元素
                index = jList.getSelectedIndex();
                return new Transferable() {
                    @Override
                    public DataFlavor[] getTransferDataFlavors() {
// 返回支持的数据类型数组
                        return new DataFlavor[]{musicFlavor};
                    }

                    @Override
                    public boolean isDataFlavorSupported(DataFlavor flavor) {
// 判断是否支持指定的数据类型
                        return musicFlavor.equals(flavor);
                    }

                    @Override
                    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {
// 返回被封装的数据对象
                        if (isDataFlavorSupported(flavor)) {
                            return jList.getSelectedValue();
                        } else {
                            throw new UnsupportedFlavorException(flavor);
                        }
                    }
                };
            }

            @Override
            public boolean canImport(TransferHandler.TransferSupport support) {
// 判断是否可以导入数据
                return support.isDataFlavorSupported(musicFlavor);
            }

            @Override
            public boolean importData(TransferHandler.TransferSupport support) {
// 导入数据
                try {
// 获取被传输的数据对象
                    Music music = (Music) support.getTransferable().getTransferData(musicFlavor);
                    int index2 = jList.getDropLocation().getIndex();
                    if (index2 < index) index++;
                    playListModel.add(index2, music);
                    playListModel.remove(index);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
        });
        try {
            BufferedImage derpy_buffer = ImageIO.read(new File("art/componentsPicture/derpy_vector.png"));
            ImageProducer derpy_source = new FilteredImageSource(derpy_buffer.getSource(), new RGBImageFilter() {
                @Override
                public int filterRGB(int x, int y, int rgb) {
                    if ((rgb & 0xff000000) == 0) return 0;
                    return (rgb & 0x00ffffff) | 0xc0000000;
                }
            });
            derpy = Toolkit.getDefaultToolkit().createImage(derpy_source);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
