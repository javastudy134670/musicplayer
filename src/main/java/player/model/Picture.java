package player.model;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class Picture extends JPanel {
    private Image image;
    public static final Image DEFAULT = new BufferedImage(70, 70, BufferedImage.TYPE_INT_RGB);

    static {
        Graphics g = DEFAULT.getGraphics();
        g.setColor(Color.GRAY);
        g.fillRect(0, 0, 70, 70);
    }

    public Picture(int len) {
        super();
        setVisible(true);
        setPreferredSize(new Dimension(len, len));
        setImage(DEFAULT);

    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Image getImage() {
        return image;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
    }
}