package player.gui.center.content;

import app.MusicPlayer;
import dao.MusicDao;
import dao.SheetDao;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import player.gui.bottom.BottomPanel;
import player.model.Music;
import player.model.Sheet;
import online.model.OnlineSheet;
import online.operation.OnlineSheetTaker;
import online.operation.FileDownloader;
import online.util.ServerURL;
import player.gui.center.ScrollBarUI;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class MusicSpace extends JPanel {
    private final JScrollPane allMusics = new JScrollPane();
    private boolean loading = false;
    private final JPanel container;
    boolean updating = false;

    public MusicSpace() {
        MusicPlayer.log("While construct %s downloading files".formatted(this.getClass().getName()));
        init();
        container = new JPanel();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setBackground(Color.GRAY);

        JButton update = new JButton("刷新");
        JButton all = new JButton("所有歌曲");
        JPanel background = new JPanel();
        JPanel buttonRow = new JPanel();
        JScrollPane scrollPane = new JScrollPane(background);
        buttonRow.setBackground(Color.GRAY);
        scrollPane.setBackground(Color.GRAY);
        scrollPane.getVerticalScrollBar().setUI(new ScrollBarUI());
        scrollPane.getHorizontalScrollBar().setUI(new ScrollBarUI());
        update.setBackground(Color.LIGHT_GRAY);
        all.setBackground(Color.LIGHT_GRAY);
        update.addActionListener(e -> update());
        all.addActionListener(e -> ContentFrame.setView("音乐广场", allMusics));
        update.setAlignmentX(0.5f);
        all.setAlignmentX(0.5f);
        background.setBackground(Color.GRAY);
        background.add(container);

        setBackground((Color.GRAY));
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        buttonRow.add(update);
        buttonRow.add(all);
        add(buttonRow);
        add(scrollPane);
        reload();
    }

    private void update() {
        if (!updating) {
            MusicPlayer.log("MusicSpace updating ...");
            Thread updateTask = new UpdateTask();
            updateTask.start();
        } else {
            MusicPlayer.log("There has been an updating task, please wait");
        }

    }

    private void reload() {
        MusicPlayer.log("MusicSpace reloading ...");
        container.removeAll();
        container.add(new JLabel("正在加载图片中，第一次加载可能比较慢..."));
        revalidate();
        var sheetList = new SheetDao().findAll();
        Collections.shuffle(sheetList, new Random());
        container.removeAll();
        boolean reachable = true;
        for (var sheet : sheetList) {
            if (reachable && !new File(sheet.picPath()).exists()) {
                int status = FileDownloader.downloadMusicSheetPicture(ServerURL.URL_DownloadMusicSheetPicture, sheet.uuid(), "pool/picture");
                if (status != 200) {
                    reachable = false;
                    MusicPlayer.log("It seems that you have not connected to the Internet yet");
                }
            }
            SheetItem sheetItem = new SheetItem(sheet);
            sheetItem.goBack(goBack);
            sheetItem.goToDetail(goToDetail);
            container.add(sheetItem);
        }
        if (sheetList.isEmpty()) {
            container.add(new JLabel("空空如也！"));
            container.add(new JLabel("不妨点击刷新加载歌单吧！"));
        }
        revalidate();
    }

    private final ActionListener goBack = e -> {
        ContentFrame.setView("音乐广场", this);
        revalidate();
    };

    private final SheetItem.SheetItemListener goToDetail = source -> {
        ContentFrame.setView("音乐广场", source);
        revalidate();
    };

    private void init() {
        DefaultTableModel model = new DefaultTableModel();
        Vector<Music> collection = new Vector<>();
        JPanel panel = new JPanel();
        JPanel buttonsRow = new JPanel();
        JButton back = new JButton("返回");
        JButton playAll = new JButton("播放全部");
        JButton load = new JButton("加载");
        JLabel status = new JLabel("试试加载所有音乐");
        JTable table = new JTable(model) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane scrollPane = new JScrollPane(table);
        status.setAlignmentX(0.5f);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBackground(Color.GRAY);
        buttonsRow.setBackground(Color.GRAY);
        back.setBackground(Color.LIGHT_GRAY);
        playAll.setBackground(Color.LIGHT_GRAY);
        load.setBackground(Color.LIGHT_GRAY);
        buttonsRow.add(back);
        buttonsRow.add(load);
        buttonsRow.add(playAll);
        panel.add(buttonsRow);
        panel.add(status);
        panel.add(scrollPane);
        scrollPane.getVerticalScrollBar().setUI(new ScrollBarUI());
        allMusics.getVerticalScrollBar().setUI(new ScrollBarUI());
        allMusics.getVerticalScrollBar().setUI(new ScrollBarUI());
        allMusics.setViewportView(panel);
        model.setColumnIdentifiers(new String[]{"收藏", "歌单", "歌曲名称", "歌手", "时长"});
        table.getColumnModel().getColumn(0).setPreferredWidth(0);
        table.getColumnModel().getColumn(4).setPreferredWidth(0);
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(JLabel.CENTER);
        for (int i = 0; i < 5; i++) {
            table.getColumnModel().getColumn(i).setCellRenderer(renderer);
        }
        back.addActionListener(goBack);
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int index = table.getSelectedRow();
                if (e.getClickCount() == 2 && index > -1) {
                    Music music = collection.get(index);
                    MusicPlayer.playList.addFront(music);
                }
            }
        });
        playAll.addActionListener(e -> {
            if (collection.isEmpty()) {
                MusicPlayer.log("[WARN]: Please load before play musics");
                status.setText("请先加载歌曲");
            } else {
                BottomPanel.reset(collection);
            }
        });
        load.addActionListener(e -> {
            MusicPlayer.log("[INFO]: MusicSpace loading musics...");
            status.setText("正在加载中，请稍后");
            if (!loading) {
                loading = true;
                new Thread(() -> {
                    model.setRowCount(0);
                    collection.clear();
                    var musicList = new MusicDao().findAll();
                    for (Music music : musicList) {
                        String artist = "Unknown artist";
                        String songName = music.name();
                        String sheetName = new SheetDao().findById(music.sheetId()).name();
                        songName = songName.substring(0, songName.lastIndexOf('.'));
                        File file = new File(music.filePath());
                        if (!file.isFile()) {
                            status.setText("正在下载歌曲：" + music.name());
                            FileDownloader.downloadMusicFile(ServerURL.URL_DownloadMusicFile, music.md5(), "pool/song", music.name());
                        }
                        MP3File mp3File;
                        try {
                            mp3File = (MP3File) AudioFileIO.read(new File(music.filePath()));
                        } catch (CannotReadException | InvalidAudioFrameException | ReadOnlyFileException |
                                 TagException |
                                 IOException exception) {
                            MusicPlayer.log("[ERROR]: music file \"%s\" can not be recognized.");
                            continue;
                        }
                        int totalTime = mp3File.getMP3AudioHeader().getTrackLength();
                        AbstractID3v2Tag v2tag = mp3File.getID3v2Tag();
                        if (v2tag != null) {
                            String gotArtist = v2tag.getFirst(FieldKey.ARTIST);
                            String gotSongName = v2tag.getFirst(FieldKey.TITLE);
                            if (!gotArtist.isBlank()) artist = gotArtist;
                            if (!gotSongName.isBlank()) songName = gotSongName;
                        }

                        collection.add(music);
                        model.addRow(new String[]{"否", sheetName, songName, artist, "%02d:%02d".formatted(totalTime / 60, totalTime % 60)});
                    }
                    MusicPlayer.log("[READY]: load musics finished.");
                    status.setText("加载完毕，共计%d首歌".formatted(model.getRowCount()));
                    loading = false;
                }).start();
            } else {
                MusicPlayer.log("[WARN]: There has been an load task, please wait...");
            }
        });
    }

    private void getSheetsFromServer() {
        SheetDao sheetDao = new SheetDao();
        MusicDao musicDao = new MusicDao();
        var localSheets = sheetDao.findAll();
        var localMusics = musicDao.findAll();
        Set<String> uuidSet = new HashSet<>();
        Set<String> md5Set = new HashSet<>();
        for (var localSheet : localSheets) {
            uuidSet.add(localSheet.uuid());
        }
        for (var localMusic : localMusics) {
            md5Set.add(localMusic.md5());
        }
        try {
            var onlineSheets = OnlineSheetTaker.queryOnlineSheets(ServerURL.URL_QueryAllMusicSheets, 80);
            for (var onlineSheetName : onlineSheets.keySet()) {
                OnlineSheet onlineSheet = onlineSheets.get(onlineSheetName);
                String onlineUuid = onlineSheet.getUuid();
                if (!uuidSet.contains(onlineUuid)) {
                    Sheet sheet = new Sheet(
                            0,
                            onlineUuid,
                            onlineSheetName,
                            onlineSheet.getCreator(),
                            onlineSheet.getDateCreated(),
                            "pool/picture/" + onlineSheet.getPicture());
                    sheetDao.insert(sheet);
                    var onlineMusics = onlineSheet.getMusicItems();
                    for (var onlineMd5 : onlineMusics.keySet()) {
                        if (!md5Set.contains(onlineMd5)) {
                            String onlineMusicName = onlineMusics.get(onlineMd5);
                            Music music = new Music(
                                    0,
                                    onlineMusicName,
                                    sheetDao.getId(onlineUuid),
                                    onlineMd5,
                                    "pool/song/" + onlineMusicName
                            );
                            musicDao.insert(music);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    class UpdateTask extends Thread {
        @Override
        public void run() {
            updating = true;
            container.removeAll();
            container.add(new JLabel("updating..."));
            revalidate();
            getSheetsFromServer();
            reload();
            updating = false;
            MusicPlayer.log("[READY]: MusicSpace update finished.");
        }
    }
}
