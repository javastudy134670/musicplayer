package player.gui.center.content;

import app.MusicPlayer;
import dao.MusicDao;
import player.model.Music;
import player.model.Sheet;
import online.operation.FileDownloader;
import online.util.ServerURL;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import player.gui.bottom.BottomPanel;
import player.model.Picture;
import player.gui.center.ScrollBarUI;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

public class SheetDetail extends JScrollPane {
    private final Sheet sheet;
    private final JLabel title = new JLabel();
    private final JLabel creator = new JLabel();
    private final JLabel date = new JLabel();
    private final JLabel status = new JLabel("试试点击加载吧！");
    private final Picture pictureBig = new Picture(80);
    private final MusicTableModel model = new MusicTableModel();
    private final JButton back = new JButton("返回");
    private boolean loading = false;

    @Override
    protected void paintComponent(Graphics g) {
        title.setText(sheet.name());
        creator.setText(sheet.creator());
        date.setText(sheet.dateCreated());
        if (new File(sheet.picPath()).isFile()) {
            pictureBig.setImage(new ImageIcon(sheet.picPath()).getImage());
        }
        super.paintComponent(g);
    }

    SheetDetail(Sheet sheet) {
        this.sheet = sheet;
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JPanel top = new JPanel();
        JPanel top_right = new JPanel();
        JPanel buttonsRow = new JPanel();
        JTable table = new JTable(model) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        JScrollPane tablePane = new JScrollPane(table);
        JButton like = new JButton("收藏歌单");
        JButton load = new JButton("加载");
        JButton playSheet = new JButton("播放歌单");
        table.getColumnModel().getColumn(0).setPreferredWidth(0);
        table.getColumnModel().getColumn(3).setPreferredWidth(0);
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(JLabel.CENTER);
        for (int i = 0; i < 4; i++) {
            table.getColumnModel().getColumn(i).setCellRenderer(renderer);
        }
        top_right.setLayout(new BoxLayout(top_right, BoxLayout.Y_AXIS));
        top.add(pictureBig);
        top.add(top_right);
        top_right.add(title);
        top_right.add(creator);
        top_right.add(date);
        buttonsRow.add(back);
        buttonsRow.add(load);
        buttonsRow.add(like);
        buttonsRow.add(playSheet);
        panel.add(top);
        panel.add(buttonsRow);
        panel.add(status);
        panel.add(tablePane);
        setViewportView(panel);
        status.setAlignmentX(0.5f);
        setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        getVerticalScrollBar().setUI(new ScrollBarUI());
        getHorizontalScrollBar().setUI(new ScrollBarUI());
        setBackground(Color.GRAY);
        top.setBackground(Color.GRAY);
        top_right.setBackground(Color.GRAY);
        buttonsRow.setBackground(Color.GRAY);
        panel.setBackground(Color.GRAY);
        tablePane.setBackground(Color.GRAY);
        back.setBackground(Color.LIGHT_GRAY);
        load.setBackground(Color.LIGHT_GRAY);
        like.setBackground(Color.LIGHT_GRAY);
        playSheet.setBackground(Color.LIGHT_GRAY);
        load.addActionListener(e -> loadMusics());
        playSheet.addActionListener(e -> {
            if (model.musics.isEmpty()) {
                MusicPlayer.log("[WARN]: Please load before play sheet");
                status.setText("请先加载歌单");
            } else {
                BottomPanel.reset(model.musics);
            }
        });
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int index = table.getSelectedRow();
                if (e.getClickCount() == 2 && index > -1) {
                    Music music = model.musics.get(index);
                    MusicPlayer.playList.addFront(music);
                }
            }
        });
    }

    void goBack(ActionListener listener) {
        back.addActionListener(listener);
    }

    void loadMusics() {
        MusicPlayer.log("[INFO]: MusicSpace loading musics...");
        status.setText("正在加载中，请稍后");
        if (!loading) {
            loading = true;
            new Thread(() -> {
                model.clear();
                var musicList = new MusicDao().findBySheetId(sheet.id());
                musicList.forEach(model::add);
                MusicPlayer.log("[READY]: load musics finished.");
                status.setText("加载完毕，共计%d首歌".formatted(model.getRowCount()));
                loading = false;
            }).start();
        } else {
            MusicPlayer.log("[WARN]: There has been an load task, please wait...");
        }
    }

    class MusicTableModel extends DefaultTableModel {
        final Vector<Music> musics = new Vector<>();

        MusicTableModel() {
            setColumnIdentifiers(new String[]{"收藏", "歌曲名称", "歌手", "时长"});
        }

        public void clear() {
            setRowCount(0);
            musics.clear();
        }

        public void add(Music music) {
            String artist = "Unknown artist";
            String songName = music.name();
            songName = songName.substring(0, songName.lastIndexOf('.'));
            File file = new File(music.filePath());
            if (!file.isFile()) {
                status.setText("正在下载歌曲：" + music.name());
                FileDownloader.downloadMusicFile(ServerURL.URL_DownloadMusicFile, music.md5(), "pool/song", music.name());
            }
            MP3File mp3File;
            try {
                mp3File = (MP3File) AudioFileIO.read(new File(music.filePath()));
            } catch (CannotReadException | InvalidAudioFrameException | ReadOnlyFileException | TagException |
                     IOException e) {
                MusicPlayer.log("[ERROR]: music file \"%s\" can not be recognized.".formatted(songName));
                return;
            }
            int totalTime = mp3File.getMP3AudioHeader().getTrackLength();
            AbstractID3v2Tag v2tag = mp3File.getID3v2Tag();
            if (v2tag != null) {
                String gotArtist = v2tag.getFirst(FieldKey.ARTIST);
                String gotSongName = v2tag.getFirst(FieldKey.TITLE);
                if (!gotArtist.isEmpty()) artist = gotArtist;
                if (!gotSongName.isEmpty()) songName = gotSongName;
            }
            musics.add(music);
            addRow(new String[]{"否", songName, artist, "%02d:%02d".formatted(totalTime / 60, totalTime % 60)});
        }

    }
}