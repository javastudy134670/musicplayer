package player.gui.center.content;

import javax.swing.*;
import java.awt.*;

public class ContentFrame extends JInternalFrame {
    private static final Container musicSpace = new DefaultPanel();
    private static final Container localDownload = new DefaultPanel();
    private static final Container myFavorite = new DefaultPanel();

    public static void setView(String name, Component view) {
        Container target;
        switch (name) {
            case "音乐广场" -> target = musicSpace;
            case "本地下载" -> target = localDownload;
            case "我的收藏" -> target = myFavorite;
            default -> {
                return;
            }
        }
        target.removeAll();
        target.add(view);
        target.repaint();
    }

    public ContentFrame() {
        super("我的音乐", true, true, true, true);
        setBounds(50, 50, 300, 200);
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("音乐广场", musicSpace);
        tabbedPane.addTab("本地下载", localDownload);
        tabbedPane.addTab("我的收藏", myFavorite);
        setView("音乐广场", new MusicSpace());
        add(tabbedPane);
        setVisible(false);
        setBackground(Color.GRAY);
    }

    @Override
    public void doDefaultCloseAction() {
        setVisible(false);
    }

    static class DefaultPanel extends JPanel {
        DefaultPanel() {
            super(new BorderLayout());
            setBackground(Color.LIGHT_GRAY);
        }
    }

}
