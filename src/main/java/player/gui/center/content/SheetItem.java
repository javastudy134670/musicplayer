package player.gui.center.content;

import player.model.Sheet;
import player.model.Picture;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

public class SheetItem extends JPanel {
    private final Picture picture = new Picture(60);
    private final JLabel name = new JLabel();
    private final JLabel creator = new JLabel();
    private final JLabel date = new JLabel();
    private SheetItemListener listener = null;
    private final SheetDetail sheetDetail;
    private final Sheet sheet;

    public SheetItem(Sheet sheet) {
        this.sheet = sheet;
        sheetDetail = new SheetDetail(sheet);
        JPanel sheetInfoPane = new JPanel();
        sheetInfoPane.setLayout(new BoxLayout(sheetInfoPane, BoxLayout.Y_AXIS));
        sheetInfoPane.add(name);
        sheetInfoPane.add(creator);
        sheetInfoPane.add(date);
        picture.setBounds(0, 0, 60, 60);
        add(picture);
        add(sheetInfoPane);
        sheetInfoPane.setOpaque(false);
        setBackground(Color.WHITE);
        setBorder(new LineBorder(Color.BLACK));
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                setBackground(Color.LIGHT_GRAY);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setBackground(Color.WHITE);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (listener != null)
                    listener.performed(sheetDetail);
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        name.setText("歌单名：" + sheet.name());
        creator.setText("创建者：" + sheet.creator());
        date.setText("创建日期：" + sheet.dateCreated());
        if (new File(sheet.picPath()).isFile()) {
            picture.setImage(new ImageIcon(sheet.picPath()).getImage());
        }
        super.paintComponent(g);
    }

    public void goToDetail(SheetItemListener listener1) {
        listener = listener1;
    }

    public void goBack(ActionListener listener) {
        sheetDetail.goBack(listener);
    }

    public interface SheetItemListener {
        void performed(SheetDetail source);
    }

}
