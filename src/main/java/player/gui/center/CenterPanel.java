package player.gui.center;

import app.MusicPlayer;
import player.gui.center.content.ContentFrame;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.beans.PropertyVetoException;

/**
 * 中心面板
 * 包含壁纸、播放列表、内部窗口。
 *
 * @author 李兴男、罗浩
 */
public class CenterPanel extends JPanel {
    /**
     * 内部窗口
     *
     * @see ContentFrame
     */
    private static final ContentFrame contentFrame = new ContentFrame();

    /**
     * 调出内部窗口。
     */
    public static void turnOnContentFrame() {
        contentFrame.setVisible(true);
        try {
            contentFrame.setMaximum(true);
        } catch (PropertyVetoException ignore) {
        }
    }

    /**
     * 壁纸
     *
     * @see WallPaper
     */
    private final WallPaper wallPaper = new WallPaper();

    /**
     * 从{@link  MusicPlayer}取得playlist，构造中央面板。
     */
    public CenterPanel() {
        MusicPlayer.log("While construct " + this.getClass().getName());

        JScrollPane playList = new JScrollPane(MusicPlayer.playList.getjList());
        setBackground(Color.LIGHT_GRAY);
        setLayout(new BorderLayout(0, 0));
        wallPaper.add(contentFrame);
        add(wallPaper, BorderLayout.CENTER);
        add(playList, BorderLayout.WEST);
        playList.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        playList.setBorder(new TitledBorder("播放列表"));
        playList.setPreferredSize(new Dimension(100, this.getHeight()));
        playList.getVerticalScrollBar().setUI(new ScrollBarUI());
    }

    public WallPaper getWallPaper() {
        return wallPaper;
    }

}
