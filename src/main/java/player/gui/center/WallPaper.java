package player.gui.center;

import app.MusicPlayer;

import javax.swing.*;
import java.awt.*;
import java.io.File;

public class WallPaper extends JDesktopPane {
    private static final File[] wallPapers = new File("art/wallPapers").listFiles();
    private static Image[] pictures;
    private static int index = 0;

    public WallPaper() {
        MusicPlayer.log("While construct " + this.getClass().getName());

        setBackground(new Color(87, 89, 87));
        File[] wallPapers = new File("art/wallPapers").listFiles();
        assert wallPapers != null;
        pictures = new Image[wallPapers.length];
        for (int i = 0; i < wallPapers.length; i++) {
            pictures[i] = new ImageIcon(wallPapers[i].getPath()).getImage();
        }
    }

    public void nextPicture() {
        index = (index + 1) % wallPapers.length;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int height = pictures[index].getHeight(this);
        int width = pictures[index].getWidth(this);
        int actualWidth = width * getHeight() / height;
        g.drawImage(pictures[index], 0, 0, actualWidth, getHeight(), this);
    }

}
