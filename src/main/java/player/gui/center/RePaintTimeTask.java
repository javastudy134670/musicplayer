package player.gui.center;

import java.util.TimerTask;

public class RePaintTimeTask extends TimerTask {
    WallPaper wallPaper;

    public RePaintTimeTask(WallPaper wallPaper) {
        super();
        this.wallPaper = wallPaper;
    }

    @Override
    public void run() {
        wallPaper.nextPicture();
        wallPaper.repaint();
    }
}
