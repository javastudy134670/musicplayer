package player.gui.center;

import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollBarUI;
import java.awt.*;

public class ScrollBarUI extends BasicScrollBarUI {

    // 重写getPreferredSize方法，返回一个固定的维度对象
    @Override
    public Dimension getPreferredSize(JComponent c) {
        return new Dimension(7, 7);
    }

    // 重写paintTrack方法，绘制滚动条轨道
    @Override
    protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
        Color gray = new Color(140, 145, 150, 200);
// 设置轨道背景颜色为灰色
        g.setColor(gray);
// 填充轨道区域
        g.fillRect(trackBounds.x, trackBounds.y, trackBounds.width, trackBounds.height);
// 设置轨道边框颜色为灰色
        g.setColor(gray);
// 绘制轨道边框
        g.drawRect(trackBounds.x, trackBounds.y, trackBounds.width - 1, trackBounds.height - 1);
    }

    // 重写paintThumb方法，绘制滚动条滑块
    @Override
    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
// 设置滑块背景颜色为灰色
        g.setColor(Color.LIGHT_GRAY);
// 填充滑块区域
        g.fillRect(thumbBounds.x, thumbBounds.y, thumbBounds.width, thumbBounds.height);
// 设置滑块边框颜色为深灰色
        g.setColor(Color.LIGHT_GRAY);
// 绘制滑块边框
        g.drawRect(thumbBounds.x, thumbBounds.y, thumbBounds.width - 1, thumbBounds.height - 1);
    }

    @Override
    protected JButton createDecreaseButton(int orientation) {
        return new JButton() {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(0, 0);
            }
        };
    }

    @Override
    protected JButton createIncreaseButton(int orientation) {
        return new JButton() {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(0, 0);
            }
        };
    }
}
