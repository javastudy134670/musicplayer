package player.gui.bottom;

import app.MusicPlayer;
import player.model.Picture;

import javax.swing.*;
import java.awt.*;

public class DetailPanel extends JPanel {
    private final Picture picture;
    private final JLabel songName;
    private final JLabel singer;

    public DetailPanel() {
        super();
        MusicPlayer.log("While construct " + this.getClass().getName());

        picture = new Picture(50);
        songName = new JLabel("Unknown Song");
        singer = new JLabel("Unknown Singer");
        singer.setFont(new Font(null, Font.BOLD, 8));
        JPanel infoPanel = new JPanel();
        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
        infoPanel.add(songName);
        infoPanel.add(singer);
        add(picture);
        add(infoPanel);
    }

    public void setSinger(String singerName) {
        singer.setText(singerName);
        if (singerName.length() > 20) {
            singer.setText(singerName.substring(0, 20) + "...");
        }
    }

    public void setSongName(String songName1) {
        songName.setText(songName1);
        if (songName1.length() > 20) {
            songName.setText(songName1.substring(0, 20) + "...");
        }
    }

    public void rePaintPicture(Image image) {
        picture.setImage(image);
        repaint();
    }
}
