package player.gui.bottom;

import app.MusicPlayer;

import javax.swing.JProgressBar;
import java.awt.*;

public class MusicProgressBar extends JProgressBar {
    MusicProgressBar() {
        super();
        MusicPlayer.log("While construct " + this.getClass().getName());

        setForeground(Color.GRAY);
        setValue(40);
        setPreferredSize(new Dimension(200, 8));
    }
}
