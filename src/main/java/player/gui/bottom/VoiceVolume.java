package player.gui.bottom;

import javax.swing.JProgressBar;
import java.awt.*;

public class VoiceVolume extends JProgressBar {
    VoiceVolume() {
        super();
        setForeground(Color.GRAY);
        setValue(80);
        setPreferredSize(new Dimension(70, 12));
    }
}
