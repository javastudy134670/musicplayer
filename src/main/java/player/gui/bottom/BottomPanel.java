package player.gui.bottom;

import app.MusicPlayer;
import dao.SheetDao;
import player.model.Music;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.id3.AbstractID3v2Frame;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import org.jaudiotagger.tag.id3.framebody.FrameBodyAPIC;
import player.model.Picture;
import player.gui.top.PlayerButton;
import player.model.Sheet;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

import static app.MusicPlayer.controller;
import static app.MusicPlayer.playList;

public class BottomPanel extends JPanel {
    private static int int_time = 0;
    private static final DetailPanel detailPanel = new DetailPanel();

    public static void reset(Collection<Music> musics) {
        playList.reset(musics);
        switchCurrentSong();
    }

    public static void switchSong(Music newSong) {
        File newSongFile = new File(newSong.filePath());
        if (newSongFile.exists()) {
            String fileName = newSongFile.getName();
            detailPanel.setSongName(fileName.substring(0, fileName.lastIndexOf('.')));
            trackSong(newSong);
            controller.switchMusic(newSongFile);
        }
    }

    private static void switchCurrentSong() {
        switchSong(playList.currentMusic());
    }

    private static void trackSong(Music music) {
        try {
            File file = new File(music.filePath());
            MP3File mp3File = (MP3File) AudioFileIO.read(file);
            int_time = mp3File.getMP3AudioHeader().getTrackLength();
            AbstractID3v2Tag tag = mp3File.getID3v2Tag();
            detailPanel.rePaintPicture(Picture.DEFAULT);
            Sheet sheet = new SheetDao().findById(music.sheetId());
            if (new File(sheet.picPath()).isFile()) {
                detailPanel.rePaintPicture(new ImageIcon(sheet.picPath()).getImage());
            }
            if (tag != null) {
                String singer = tag.getFirst(FieldKey.ARTIST);
                String title = tag.getFirst(FieldKey.TITLE);
                if (!singer.isBlank())
                    detailPanel.setSinger(singer);
                if (!title.isBlank())
                    detailPanel.setSongName(title);

                try {
                    AbstractID3v2Frame frame = (AbstractID3v2Frame) mp3File.getID3v2Tag().getFrame("APIC");
                    if (frame != null) {
                        FrameBodyAPIC body = (FrameBodyAPIC) frame.getBody();
                        byte[] imageData = body.getImageData();
                        detailPanel.rePaintPicture(Toolkit.getDefaultToolkit().createImage(imageData, 0, imageData.length));
                    }
                } catch (ClassCastException ignore) {
                }
            } else {
                detailPanel.setSinger("Unknown Singer");
            }
        } catch (CannotReadException | InvalidAudioFrameException | ReadOnlyFileException | TagException |
                 IOException e) {
            e.printStackTrace();
        }
    }

    private final JLabel curTime = new JLabel("00:00");
    private final JLabel totalTime = new JLabel("00:00");
    private final MusicProgressBar progressBar = new MusicProgressBar();
    private final java.util.Timer processPrinter = new Timer();

    public void setLog(JLabel log) {
        add(log, BorderLayout.SOUTH);
        revalidate();
    }

    public BottomPanel() {
        super(new BorderLayout());
        MusicPlayer.log("While construct " + this.getClass().getName());

        Color topic = new Color(192, 192, 192);
        setBackground(topic);
        PlayerButton lastMusic = new PlayerButton("lastMusic", "art/btn/play/play_last.png");
        PlayerButton play_pause = new PlayerButton("play", "art/btn/play/play_start.png");
        PlayerButton nextMusic = new PlayerButton("nextMusic", "art/btn/play/play_next.png");
        JPanel controlPanel = new JPanel();
        controlPanel.setBackground(topic);
        controlPanel.add(detailPanel);
        controlPanel.add(lastMusic);
        controlPanel.add(play_pause);
        controlPanel.add(nextMusic);
        controlPanel.add(curTime);
        controlPanel.add(progressBar);
        controlPanel.add(totalTime);
        add(controlPanel, BorderLayout.CENTER);

        MouseAdapter controlMouseAdapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (playList.isEmpty()) return;
                PlayerButton source = (PlayerButton) e.getSource();
                switch (source.getName()) {
                    case "lastMusic" -> {
                        playList.last();
                        switchCurrentSong();
                        controller.continues();
                    }
                    case "play" -> {
                        controller.continues();
                        source.assign("pause", new ImageIcon("art/btn/play/pause.png"));
                    }
                    case "pause" -> {
                        controller.pause();
                        source.assign("play", new ImageIcon("art/btn/play/play_start.png"));
                    }
                    case "nextMusic" -> {
                        playList.next();
                        switchCurrentSong();
                        controller.continues();
                    }
                }
            }
        };
        lastMusic.addMouseListener(controlMouseAdapter);
        play_pause.addMouseListener(controlMouseAdapter);
        nextMusic.addMouseListener(controlMouseAdapter);
        progressBar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controller.gotoProcess(e.getX() * controller.getTotalFrame() / progressBar.getWidth());
                controller.continues();
            }
        });

        processPrinter.schedule(new TimerTask() {
            @Override
            public void run() {
                float process = (float) controller.getProcess() / controller.getTotalFrame();
                curTime.setText("%02d:%02d".formatted((int) (int_time * process) / 60, (int) (int_time * process) % 60));
                totalTime.setText("%02d:%02d".formatted(int_time / 60, int_time % 60));
                progressBar.setValue((int) (process * 100));
                if (!controller.running()) {
                    play_pause.assign("play", new ImageIcon("art/btn/play/play_start.png"));
                } else {
                    play_pause.assign("pause", new ImageIcon("art/btn/play/pause.png"));
                }
                if (controller.isOver()) {
                    playList.next();
                    switchCurrentSong();
                    controller.continues();
                    MusicPlayer.log("[INFO]: Change to next song automatically");
                }
            }
        }, 500, 500);
    }

    public void cancel() {
        processPrinter.cancel();
    }

}
