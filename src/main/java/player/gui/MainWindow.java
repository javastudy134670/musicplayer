package player.gui;


import app.MusicPlayer;
import player.gui.bottom.BottomPanel;
import player.gui.center.CenterPanel;
import player.gui.top.PlayerButton;
import player.gui.top.TopPanel;
import player.gui.center.RePaintTimeTask;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Timer;

/**
 * 主窗口
 *
 * @author 罗浩、李兴男
 */
public class MainWindow extends JFrame {
    /**
     * 顶部面板 包含“我的音乐”以及窗口控制按钮
     *
     * @see TopPanel
     */
    private final TopPanel topPanel;
    /**
     * 中央面板
     * 承载音乐播放器信息展示的主要区域。
     * 包含播放列表、自动切换壁纸、内部窗口。
     *
     * @see CenterPanel
     */
    private final CenterPanel centerPanel;
    /**
     * 底部面板
     * 承载着播放控制的功能。展示log标签
     *
     * @see BottomPanel
     */
    private final BottomPanel bottomPanel;
    /**
     * 计时器，用于定时重绘壁纸。
     */
    private final Timer timer;
    /**
     * 壁纸重绘任务。当窗口正在移动时应当停止壁纸重绘任务，否则绘画会出现紊乱。当窗口停止移动时再重新规划重绘任务
     *
     * @see RePaintTimeTask
     * @see player.gui.center.WallPaper
     */
    private RePaintTimeTask timerTask;

    /**
     * 得到log标签，用于传递给{@link BottomPanel}
     *
     * @param log 从{@link MusicPlayer}传递来的log标签
     */
    public void setLog(JLabel log) {
        bottomPanel.setLog(log);
    }

    /**
     * 构造主窗口。设置透明度为90%
     */
    public MainWindow() {
        MusicPlayer.log("While construct " + this.getClass().getName());

        topPanel = new TopPanel();
        centerPanel = new CenterPanel();
        bottomPanel = new BottomPanel();
        timer = new Timer();
        timerTask = new RePaintTimeTask(centerPanel.getWallPaper());

        timer.schedule(timerTask, 3_000, 3_000);    //每3秒重绘一张壁纸

        setUndecorated(true);       //取消默认装饰
        setOpacity((float) 0.9);    //可见度，不透明度
        setBounds(100, 100, 640, 400);  //窗体大小
        add(centerPanel, BorderLayout.CENTER);
        add(topPanel, BorderLayout.NORTH);
        add(bottomPanel, BorderLayout.SOUTH);
        addListeners();

        MusicPlayer.log("[SUCCESS]: MusicPlayer has been launched successfully!");
    }

    /**
     * 为窗口拖动提供移动监听器，为右上角按钮设置监听器
     */
    private void addListeners() {
        topPanel.addMouseListenerForBtn(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                JLabel source = (JLabel) e.getSource();
                switch (source.getName()) {
                    case "close" -> {
                        timer.cancel(); //如果不关闭timer，则可能导致退出程序的时候进程无法终止。
                        dispose();
                    }
                    case "min" -> setExtendedState(ICONIFIED);
                    case "max" -> {
                        if (getExtendedState() == MAXIMIZED_BOTH) {
                            setBounds(100, 100, 640, 400);
                        } else {
                            setExtendedState(MAXIMIZED_BOTH);
                        }
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                PlayerButton source = (PlayerButton) e.getSource();
                source.beLight();
            }

            @Override
            public void mouseExited(MouseEvent e) {
                PlayerButton source = (PlayerButton) e.getSource();
                source.beDark();
            }
        });
        MouseAdapter dragListener = new MouseAdapter() {
            private Point lastPoint = null;

            @Override
            public void mouseReleased(MouseEvent e) {
                timerTask = new RePaintTimeTask(centerPanel.getWallPaper());
                timer.schedule(timerTask, 3_000, 3_000);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                lastPoint = e.getLocationOnScreen();
                timerTask.cancel();
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                Point point = e.getLocationOnScreen();
                int offsetX = point.x - lastPoint.x;
                int offsetY = point.y - lastPoint.y;
                Rectangle bounds = getBounds();
                bounds.x += offsetX;
                bounds.y += offsetY;
                setBounds(bounds);
                lastPoint = point;
            }
        };
        topPanel.addMouseMotionListener(dragListener);
        topPanel.addMouseListener(dragListener);
    }

    /**
     * 定义关闭行为。需要关闭所有的线程。如果不关闭，则会导致退出程序的时候进程无法终止。
     */
    @Override
    public void dispose() {
        bottomPanel.cancel();   //关闭进度追踪线程
        MusicPlayer.controller.pause(); //关闭正在播放的音乐线程
        timer.cancel(); //关闭壁纸绘画计时器
        super.dispose();
    }

}
