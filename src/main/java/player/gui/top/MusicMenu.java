package player.gui.top;

import app.MusicPlayer;
import player.gui.center.CenterPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * “我的音乐”
 * 按下时，会唤出内部窗口
 */
public class MusicMenu extends JLabel {
    public MusicMenu() {
        super();
        MusicPlayer.log("While construct " + this.getClass().getName());

        setBackground(Color.DARK_GRAY);
        setOpaque(true);
        setIcon(new ImageIcon("art/btn/Logo-96-32.png"));
        addMouseListener(new MouseAdapter() {
            /**
             * 唤出内部窗口
             * @param e the event to be processed
             */
            @Override
            public void mouseClicked(MouseEvent e) {
                CenterPanel.turnOnContentFrame();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                setBackground(Color.GRAY);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setBackground(Color.DARK_GRAY);
            }
        });
    }

}
