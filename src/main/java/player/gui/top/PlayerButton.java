package player.gui.top;

import javax.swing.*;

/**
 * UI按钮。
 * 定义了亮与不亮时的Image
 *
 * @author 李兴男
 */
public class PlayerButton extends JLabel {
    /**
     * 暗与亮的图标，当鼠标放在上面时变亮
     */
    private ImageIcon[] imageIcons;

    public PlayerButton(String name, String darkPath, String lightPath) {
        this(name, new ImageIcon(darkPath), new ImageIcon(lightPath));
    }

    public PlayerButton(String name, ImageIcon dark, ImageIcon light) {
        assign(name, dark, light);
    }

    public PlayerButton(String name, String path) {
        this(name, path, path);
    }

    public void assign(String name, ImageIcon dark, ImageIcon light) {
        setIcon(dark);
        imageIcons = new ImageIcon[]{dark, light};
        setName(name);
    }

    public void assign(String name, ImageIcon image) {
        assign(name, image, image);
    }

    /**
     * 变亮
     */
    public void beLight() {
        setIcon(imageIcons[1]);
    }

    /**
     * 变暗
     */
    public void beDark() {
        setIcon(imageIcons[0]);
    }
}
