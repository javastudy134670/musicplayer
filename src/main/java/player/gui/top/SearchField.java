package player.gui.top;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class SearchField extends JTextField {
    private String result;

    public SearchField(String text) {
        super(text, 15);
        setFont(new Font("仿宋", Font.PLAIN, 14));
        setBorder(new EmptyBorder(0, 0, 0, 0));//防止四周有多余的空白框
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        ImageIcon icon = new ImageIcon("art/btn/search.png");
        //让图片的位置更合理
        int iconWidth = icon.getIconWidth();
        int iconHeight = icon.getIconHeight();
        int Height = this.getHeight();
        icon.paintIcon(this, g, this.getWidth() - iconWidth, (Height - iconHeight) / 2);
    }

    public void updateResult() {
        result = getText();
    }

    public String getResult() {
        return result;
    }
}
