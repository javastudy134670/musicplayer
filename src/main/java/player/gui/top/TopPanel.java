package player.gui.top;

import app.MusicPlayer;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.MouseListener;

/**
 * 顶部面板 包含“我的音乐”以及窗口控制按钮
 *
 * @author 李兴男、罗浩
 */
public class TopPanel extends JPanel {

    /**
     * 关闭按钮
     */
    private final PlayerButton closeBtn = new PlayerButton("close", "art/btn/close/close_dark.png", "art/btn/close/close_light.png");
    /**
     * 最小化按钮
     */
    private final PlayerButton minBtn = new PlayerButton("min", "art/btn/min/min_dark.png", "art/btn/min/min_light.png");
    /**
     * 最大化按钮
     */
    private final PlayerButton maxBtn = new PlayerButton("max", "art/btn/max/max_dark.png", "art/btn/max/max_light.png");

    /**
     * 创建三个按钮和“我的音乐”
     *
     * @see MusicMenu
     */
    public TopPanel() {
        super();
        MusicPlayer.log("While construct " + this.getClass().getName());

        JPanel left = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel right = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        left.setBackground(Color.DARK_GRAY);
        right.setBackground(Color.DARK_GRAY);
        setBackground(Color.DARK_GRAY);
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        add(left);
        add(right);
        right.add(minBtn);
        right.add(maxBtn);
        right.add(closeBtn);
        MusicMenu musicMenu = new MusicMenu();
        left.add(musicMenu);
        SearchField textField = new SearchField("搜索歌曲");
        left.add(textField);
    }

    /**
     * 从主窗口来的监听器
     *
     * @param mouseListener 监听器
     * @see player.gui.MainWindow
     */
    public void addMouseListenerForBtn(MouseListener mouseListener) {
        closeBtn.addMouseListener(mouseListener);
        minBtn.addMouseListener(mouseListener);
        maxBtn.addMouseListener(mouseListener);
    }
}
