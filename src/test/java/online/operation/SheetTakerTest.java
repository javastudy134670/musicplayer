package online.operation;

import online.model.OnlineSheet;
import online.util.ServerURL;

import java.io.IOException;
import java.util.Collection;

import static online.operation.OnlineSheetTaker.queryOnlineSheets;

public class SheetTakerTest {

    public static void main(String[] args) throws IOException {
        Collection<OnlineSheet> onlineSheets = queryOnlineSheets(ServerURL.URL_QueryAllMusicSheets, 80).values();
        for (var sheet : onlineSheets) {
            System.out.printf("name:%20s\tcreator:%s%n",
                    sheet.getName(), sheet.getCreator()
            );
            System.out.printf("picture: %s%n", sheet.getPicture());
            for (var music : sheet.getMusicItems().values()) {
                System.out.println(music);
            }
            System.out.println();
        }

    }
}
