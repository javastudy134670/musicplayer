package player.center;

import javax.swing.*;
import java.awt.*;

public class JDesktopPaneTest extends JFrame {

    public JDesktopPaneTest() {
        super("JDesktopPane Example"); //调用父类构造方法
        setSize(400, 400); //设置窗口大小
        setDefaultCloseOperation(EXIT_ON_CLOSE); //设置窗口关闭时退出程序

        //声明一个JDesktopPane实例
        JDesktopPane desktopPane = new JDesktopPane(); //创建一个JDesktopPane实例
        desktopPane.setPreferredSize(new Dimension(400, 400)); //设置JDesktopPane的大小
        desktopPane.setBorder(BorderFactory.createTitledBorder("Click the button to swap the frames")); //设置JDesktopPane的边框

        //声明一个JInternalFrame实例
        JInternalFrame internalFrame1 = new JInternalFrame("Frame 1", true, true, true, true); //创建一个JInternalFrame实例
        internalFrame1.setBounds(50, 50, 200, 200); //设置JInternalFrame的位置和大小
        internalFrame1.setContentPane(new JLabel("This is frame 1")); //设置JInternalFrame的内容面板为一个标签
        internalFrame1.setVisible(true); //设置JInternalFrame为可见

        //声明另一个JInternalFrame实例
        JInternalFrame internalFrame2 = new JInternalFrame("Frame 2", true, true, true, true); //创建另一个JInternalFrame实例
        internalFrame2.setBounds(150, 150, 200, 200); //设置JInternalFrame的位置和大小

        JPanel panel = new JPanel();
        internalFrame2.setContentPane(panel); //设置JInternalFrame的内容面板为一个标签
        internalFrame2.setVisible(true); //设置JInternalFrame为可见

        desktopPane.add(internalFrame1); //将internalFrame1添加到desktopPane中
        desktopPane.add(internalFrame2); //将internalFrame2添加到desktopPane中

        JButton btn = new JButton("This is frame 2");
        panel.add(btn);
        btn.addActionListener(e -> {
            internalFrame2.add(new JLabel("I am here"));
            internalFrame2.revalidate();
        });

        add(desktopPane); //将desktopPane添加到窗口中

    }

    public static void main(String[] args) {
        JDesktopPaneTest example = new JDesktopPaneTest(); //创建一个JDesktopPaneExample实例
        example.setVisible(true); //设置窗口可见
    }

}