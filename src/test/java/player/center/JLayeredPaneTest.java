package player.center;

import javax.swing.*;
import java.awt.*;

public class JLayeredPaneTest extends JFrame {

    private final JLayeredPane layeredPane; //声明一个JLayeredPane实例
    private final JLabel labelA; //声明一个JLabel实例
    private final JLabel labelB; //声明另一个JLabel实例

    public JLayeredPaneTest() {
        super("JLayeredPane Example"); //调用父类构造方法
        setSize(300, 300); //设置窗口大小
        setDefaultCloseOperation(EXIT_ON_CLOSE); //设置窗口关闭时退出程序

        layeredPane = new JLayeredPane(); //创建一个JLayeredPane实例
        layeredPane.setPreferredSize(new Dimension(300, 300)); //设置JLayeredPane的大小
        layeredPane.setBorder(BorderFactory.createTitledBorder("Click the button to swap the labels")); //设置JLayeredPane的边框

        labelA = new JLabel("Label A"); //创建一个JLabel实例
        labelA.setBounds(50, 50, 100, 100); //设置JLabel的位置和大小
        labelA.setOpaque(true); //设置JLabel为不透明
        labelA.setBackground(Color.RED); //设置JLabel的背景颜色为红色

        labelB = new JLabel("Label B"); //创建另一个JLabel实例
        labelB.setBounds(100, 100, 100, 100); //设置JLabel的位置和大小
        labelB.setOpaque(true); //设置JLabel为不透明
        labelB.setBackground(Color.BLUE); //设置JLabel的背景颜色为蓝色

        //声明一个JButton实例
        JButton button = new JButton("Swap"); //创建一个JButton实例
        button.setBounds(100, 250, 100, 30); //设置JButton的位置和大小
        //给JButton添加动作监听器
        button.addActionListener(e -> { //当按钮被点击时执行以下操作
            int layerA = JLayeredPane.getLayer(labelA); //获取labelA的层级
            int layerB = JLayeredPane.getLayer(labelB); //获取labelB的层级
            layeredPane.setLayer(labelA, layerB); //将labelA的层级设为labelB的层级
            layeredPane.setLayer(labelB, layerA); //将labelB的层级设为labelA的层级
            repaint();
        });

        layeredPane.add(labelA, Integer.valueOf(1)); //将labelA添加到第一层
        layeredPane.add(labelB, Integer.valueOf(2)); //将labelB添加到第二层
        layeredPane.add(button, Integer.valueOf(3)); //将button添加到第三层

        add(layeredPane); //将layeredPane添加到窗口中
        pack();

    }

    public static void main(String[] args) {
        JLayeredPaneTest example = new JLayeredPaneTest(); //创建一个JLayeredPaneExample实例
        example.setDefaultCloseOperation(EXIT_ON_CLOSE);
        example.setVisible(true); //设置窗口可见
    }

}