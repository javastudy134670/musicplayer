package player.content;

import dao.SheetDao;
import player.gui.center.content.SheetItem;

import javax.swing.*;
import java.awt.*;

public class SheetDetailTest {
    static JFrame frame = new JFrame("SheetDetailTest");


    public static void main(String[] args) {
        JPanel background = new JPanel();
        SheetItem sheetItem = new SheetItem(new SheetDao().findById(1));

        frame.setContentPane(background);
        background.add(sheetItem);

        sheetItem.goBack(e -> {
            frame.setContentPane(background);
            frame.revalidate();
        });
        sheetItem.goToDetail(source -> {
            frame.setContentPane(source);
            frame.revalidate();
        });
        background.setBackground(Color.GRAY);
        frame.setBounds(100, 100, 400, 400);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
