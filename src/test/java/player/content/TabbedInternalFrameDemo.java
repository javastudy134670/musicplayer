package player.content;

import javax.swing.*;
import java.awt.*;

public class TabbedInternalFrameDemo extends JFrame {

    public TabbedInternalFrameDemo() {
        super("Tabbed Internal Frame Demo");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // 创建一个桌面窗格
        JDesktopPane desktop = new JDesktopPane();
        desktop.setBackground(Color.LIGHT_GRAY);

// 创建一个内部框架
        JInternalFrame internalFrame = new JInternalFrame("Tabbed Internal Frame", true, true, true, true);
        internalFrame.setSize(400, 300);
        internalFrame.setLocation(100, 100);
        internalFrame.setVisible(true);

// 创建一个标签面板
        JTabbedPane tabbedPane = new JTabbedPane();

// 创建一些组件并添加到标签面板的选项卡中
        JLabel label1 = new JLabel("This is the first tab");
        label1.setHorizontalAlignment(JLabel.CENTER);
        tabbedPane.addTab("First", label1);

        JLabel label2 = new JLabel("This is the second tab");
        label2.setHorizontalAlignment(JLabel.CENTER);
        tabbedPane.addTab("Second", label2);

        JLabel label3 = new JLabel("This is the third tab");
        label3.setHorizontalAlignment(JLabel.CENTER);
        tabbedPane.addTab("Third", label3);

// 将标签面板添加到内部框架的内容窗格中
        internalFrame.add(tabbedPane);

// 将内部框架添加到桌面窗格中
        desktop.add(internalFrame);

// 将桌面窗格设置为内容窗格
        setContentPane(desktop);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                TabbedInternalFrameDemo demo = new TabbedInternalFrameDemo();
                demo.setVisible(true);
            }
        });
    }
}