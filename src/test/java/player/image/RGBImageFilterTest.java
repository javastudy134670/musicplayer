/**
 * @(#)AlphaImageIcon.java 1.0 08/16/10
 */
package player.image;

import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * An Icon  wrapper that paints the contained icon with a specified transparency.
 * <p>
 * This class is suitable for wrapping an <CODE>ImageIcon</CODE>
 * that holds an animated image.  To show a non-animated Icon with transparency,
 *
 * @author Darryl
 * @version 1.0 08/16/10
 */
public class RGBImageFilterTest extends ImageIcon {

    public static void main(String[] args) {
        JFrame frame = new JFrame("test for AlphaImageIcon");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setContentPane(new WalPaperTmp());
        frame.setVisible(true);
    }

    static class WalPaperTmp extends JPanel {
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            ImageIcon wallpaper = new ImageIcon("art/wallPapers/6.png");
            BufferedImage derpy;
            try {
                derpy = ImageIO.read(new File("art/componentsPicture/derpy_vector.png"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            ImageProducer derpy_source = new FilteredImageSource(derpy.getSource(), new RGBImageFilter() {
                @Override
                public int filterRGB(int x, int y, int rgb) {
                    if ((rgb & 0xff000000) == 0) return 0;
                    return (rgb & 0x00ffffff) | 0xcc000000;
                }
            });
            g.drawImage(wallpaper.getImage(), 0, 0, getWidth(), getHeight() * wallpaper.getIconWidth() / wallpaper.getIconWidth(), this);
            g.drawImage(Toolkit.getDefaultToolkit().createImage(derpy_source), 0, 0, getWidth(), getHeight(), this);
        }
    }
}